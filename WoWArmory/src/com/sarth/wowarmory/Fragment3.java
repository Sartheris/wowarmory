package com.sarth.wowarmory;

import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.models.CharInfo;
import com.sarth.wowarmory.models.TalentItem;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Fragment3 extends Fragment implements JSONParserCallback {

	private int talentPosition;
	private int operationId;
	private JSONGet taskGet;
	private PopupWindow popup;
	private View rootView;
	private String imgUrl;
	private ImageView ivTier1Image1, ivTier1Image2, ivTier1Image3;
	private ImageView ivTier2Image1, ivTier2Image2, ivTier2Image3;
	private ImageView ivTier3Image1, ivTier3Image2, ivTier3Image3;
	private ImageView ivTier4Image1, ivTier4Image2, ivTier4Image3;
	private ImageView ivTier5Image1, ivTier5Image2, ivTier5Image3;
	private ImageView ivTier6Image1, ivTier6Image2, ivTier6Image3;
	private TextView tvTier1Talent1, tvTier1Talent2, tvTier1Talent3;
	private TextView tvTier2Talent1, tvTier2Talent2, tvTier2Talent3;
	private TextView tvTier3Talent1, tvTier3Talent2, tvTier3Talent3;
	private TextView tvTier4Talent1, tvTier4Talent2, tvTier4Talent3;
	private TextView tvTier5Talent1, tvTier5Talent2, tvTier5Talent3;
	private TextView tvTier6Talent1, tvTier6Talent2, tvTier6Talent3;

	public Fragment3() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		rootView = inflater.inflate(R.layout.fragment3, container, false);
		initViews(rootView);
		setOnClickListeners();
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (!menuVisible) {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
			if (popup != null) {
				popup.dismiss();
			}
		} else {
			if (!Commons.INST.fragment3done) {
				operationId = 1;
				String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/data/talents";
				taskGet = new JSONGet(this, adres);
				taskGet.execute();
			}
		}
	}

	private void setOnClickListeners() {
		setViewOnClickListener(ivTier1Image1, "1");
		setViewOnClickListener(ivTier1Image2, "2");
		setViewOnClickListener(ivTier1Image3, "3");
		setViewOnClickListener(ivTier2Image1, "4");
		setViewOnClickListener(ivTier2Image2, "5");
		setViewOnClickListener(ivTier2Image3, "6");
		setViewOnClickListener(ivTier3Image1, "7");
		setViewOnClickListener(ivTier3Image2, "8");
		setViewOnClickListener(ivTier3Image3, "9");
		setViewOnClickListener(ivTier4Image1, "10");
		setViewOnClickListener(ivTier4Image2, "11");
		setViewOnClickListener(ivTier4Image3, "12");
		setViewOnClickListener(ivTier5Image1, "13");
		setViewOnClickListener(ivTier5Image2, "14");
		setViewOnClickListener(ivTier5Image3, "15");
		setViewOnClickListener(ivTier6Image1, "16");
		setViewOnClickListener(ivTier6Image2, "17");
		setViewOnClickListener(ivTier6Image3, "18");
	}

	private void setViewOnClickListener(ImageView iv, final String number) {
		iv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showPopup(v, number);
			}
		});
	}

	/** Show pop up window */
	public void showPopup(View anchorView, String s) {
		LayoutInflater li = (LayoutInflater) getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = li.inflate(R.layout.popup_talent, null);
		popup = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popup.setBackgroundDrawable(new ColorDrawable());
		popup.setFocusable(true);
		int location[] = new int[2];
		anchorView.getLocationOnScreen(location);
		popup.showAtLocation(anchorView, Gravity.NO_GRAVITY, location[0], location[1] + anchorView.getHeight());
		popupView.setOnTouchListener(new OnTouchListener() {
			/** Popup Window Drag Event */
			int dx = 0;
			int dy = 0;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					dx = (int) event.getX();
					dy = (int) event.getY();
					break;
				case MotionEvent.ACTION_MOVE:
					int x = (int) event.getRawX();
					int y = (int) event.getRawY();
					int left = (x - dx);
					int top = (y - dy);
					popup.update(left, top, -1, -1);
					break;
				case MotionEvent.ACTION_CANCEL:
					popup.dismiss();
					break;
				}
				return true;
			}
		});
		setTalentPopupContent(popupView, s);
	}

	private void setTalentPopupContent(View popupView, String s) {
		TalentItem ti = Commons.currentCharacter.talentItem.get(s);
		TextView tvtalentTitle = (TextView) popupView.findViewById(R.id.tvtalentTitle);
		TextView tvtalentDescription = (TextView) popupView.findViewById(R.id.tvtalentDescription);
		TextView tvtalentCastTime = (TextView) popupView.findViewById(R.id.tvtalentCastTime);
		TextView tvtalentCooldown = (TextView) popupView.findViewById(R.id.tvtalentCooldown);
		TextView tvtalentLevel = (TextView) popupView.findViewById(R.id.tvtalentLevel);
		TextView tvtalentPowerCost = (TextView) popupView.findViewById(R.id.tvtalentPowerCost);
		TextView tvtalentRange = (TextView) popupView.findViewById(R.id.tvtalentRange);
		tvtalentTitle.setText(ti.talentName);
		tvtalentDescription.setText(ti.talentDescription);
		if (ti.talentPowerCost != null) {
			tvtalentPowerCost.setText(ti.talentPowerCost);
			tvtalentPowerCost.setVisibility(View.VISIBLE);
		} else {
			tvtalentPowerCost.setVisibility(View.GONE);
		}
		if (ti.talentCastTime != null) {
			tvtalentCastTime.setText(ti.talentCastTime);
			tvtalentCastTime.setVisibility(View.VISIBLE);
		} else {
			tvtalentCastTime.setVisibility(View.GONE);
		}
		if (ti.talentCooldown != null) {
			tvtalentCooldown.setText(ti.talentCooldown);
			tvtalentCooldown.setVisibility(View.VISIBLE);
		} else {
			tvtalentCooldown.setVisibility(View.GONE);
		}
		if (ti.talentRange != null) {
			tvtalentRange.setText(ti.talentRange);
			tvtalentRange.setVisibility(View.VISIBLE);
		} else {
			tvtalentRange.setVisibility(View.GONE);
		}

		int i = Integer.parseInt(s);
		/** Check Talent Level */
		if (i >= 1 && i <= 3) {
			tvtalentLevel.setText("Requires level 15");
		} else if (i >= 4 && i <= 6) {
			tvtalentLevel.setText("Requires level 30");
		} else if (i >= 7 && i <= 9) {
			tvtalentLevel.setText("Requires level 45");
		} else if (i >= 10 && i <= 12) {
			tvtalentLevel.setText("Requires level 60");
		} else if (i >= 13 && i <= 15) {
			tvtalentLevel.setText("Requires level 75");
		} else if (i >= 16 && i <= 18) {
			tvtalentLevel.setText("Requires level 90");
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		switch (operationId) {
		case 1:
			try {
				if (jsonGet != null) {
					getStuffFromJson(jsonGet);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			break;
		case 2:
			try {
				if (jsonGet != null) {
					highlightCharacterTalents(jsonGet);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}

	}

	private void highlightCharacterTalents(JSONObject json) throws JSONException {
		JSONArray jArr = json.getJSONArray("talents");
		JSONObject jj = null;
		if (jArr.getJSONObject(0).has("selected")) {
			jj = (JSONObject) jArr.get(0);
		} else if (jArr.getJSONObject(1).has("selected")) {
			jj = (JSONObject) jArr.get(1);
		} else if (jArr.getJSONObject(2).has("selected")) {
			jj = (JSONObject) jArr.get(2);
		}
		JSONArray jsonA = jj.getJSONArray("talents");
		for (int i = 0; i < jsonA.length(); i++) {
			JSONObject jsonGet = jsonA.getJSONObject(i);
			int tier = jsonGet.getInt("tier");
			int column = jsonGet.getInt("column");
			selectSingleTalent(tier, column);
		}
	}

	private void selectSingleTalent(int tier, int column) {
		switch (tier) {
		case 0:
			if (column == 0) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal11);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 1) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal12);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 2) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal13);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			}
			break;
		case 1:
			if (column == 0) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal21);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 1) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal22);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 2) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal23);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			}
			break;
		case 2:
			if (column == 0) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal31);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 1) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal32);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 2) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal33);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			}
			break;
		case 3:
			if (column == 0) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal41);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 1) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal42);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 2) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal43);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			}
			break;
		case 4:
			if (column == 0) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal51);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 1) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal52);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 2) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal53);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			}
			break;
		case 5:
			if (column == 0) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal61);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 1) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal62);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			} else if (column == 2) {
				RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.llTal63);
				rl.setBackgroundResource(R.drawable.talbacknewselected);
			}
			break;

		default:
			break;
		}
	}

	private void getStuffFromJson(JSONObject json) throws JSONException {
		JSONObject jsonGet = json.getJSONObject(String.valueOf(Commons.INST.charClassId));
		JSONArray jArrAllTallents = (JSONArray) jsonGet.getJSONArray("talents");
		Commons.currentCharacter = new CharInfo();
		if (Commons.currentCharacter.talentItem == null) {
			Commons.currentCharacter.talentItem = new HashMap<String, TalentItem>();
		}
		talentPosition = 0;
		for (int i = 0; i < jArrAllTallents.length(); i++) {
			JSONArray jArrTier0 = (JSONArray) jArrAllTallents.get(i);
			for (int j = 0; j < jArrTier0.length(); j++) {
				TalentItem ti = new TalentItem();
				JSONObject jj = (JSONObject) jArrTier0.get(j);
				ti.talentName = jj.getJSONObject("spell").getString("name");
				ti.talentDescription = jj.getJSONObject("spell").getString("description");
				ti.talentCastTime = jj.getJSONObject("spell").getString("castTime");
				if (jj.getJSONObject("spell").has("cooldown")) {
					ti.talentCooldown = jj.getJSONObject("spell").getString("cooldown");
				}
				if (jj.getJSONObject("spell").has("powerCost")) {
					ti.talentPowerCost = jj.getJSONObject("spell").getString("powerCost");
				}
				if (jj.getJSONObject("spell").has("range")) {
					ti.talentRange = jj.getJSONObject("spell").getString("range");
				}
				ti.talentIconSlug = jj.getJSONObject("spell").getString("icon");
				imgUrl = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/" + ti.talentIconSlug + ".jpg";
				talentPosition++;
				loadTalent(ti.talentName);
				String currentTalent = String.valueOf(talentPosition);
				Commons.currentCharacter.talentItem.put(currentTalent, ti);
			}
		}
		operationId = 2;
		String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/"
				+ Commons.INST.charName + "?fields=talents" + "&locale=" + Commons.INST.locale;
		taskGet = new JSONGet(this, adres);
		taskGet.execute();
	}

	private void loadTalent(String talentName) {
		switch (talentPosition) {
		case 1:
			displayTalentImage(ivTier1Image1, tvTier1Talent1, talentName);
			break;
		case 2:
			displayTalentImage(ivTier1Image2, tvTier1Talent2, talentName);
			break;
		case 3:
			displayTalentImage(ivTier1Image3, tvTier1Talent3, talentName);
			break;
		case 4:
			displayTalentImage(ivTier2Image1, tvTier2Talent1, talentName);
			break;
		case 5:
			displayTalentImage(ivTier2Image2, tvTier2Talent2, talentName);
			break;
		case 6:
			displayTalentImage(ivTier2Image3, tvTier2Talent3, talentName);
			break;
		case 7:
			displayTalentImage(ivTier3Image1, tvTier3Talent1, talentName);
			break;
		case 8:
			displayTalentImage(ivTier3Image2, tvTier3Talent2, talentName);
			break;
		case 9:
			displayTalentImage(ivTier3Image3, tvTier3Talent3, talentName);
			break;
		case 10:
			displayTalentImage(ivTier4Image1, tvTier4Talent1, talentName);
			break;
		case 11:
			displayTalentImage(ivTier4Image2, tvTier4Talent2, talentName);
			break;
		case 12:
			displayTalentImage(ivTier4Image3, tvTier4Talent3, talentName);
			break;
		case 13:
			displayTalentImage(ivTier5Image1, tvTier5Talent1, talentName);
			break;
		case 14:
			displayTalentImage(ivTier5Image2, tvTier5Talent2, talentName);
			break;
		case 15:
			displayTalentImage(ivTier5Image3, tvTier5Talent3, talentName);
			break;
		case 16:
			displayTalentImage(ivTier6Image1, tvTier6Talent1, talentName);
			break;
		case 17:
			displayTalentImage(ivTier6Image2, tvTier6Talent2, talentName);
			break;
		case 18:
			displayTalentImage(ivTier6Image3, tvTier6Talent3, talentName);
			break;
		default:
			break;
		}
		Commons.INST.fragment3done = true;
	}

	private void displayTalentImage(ImageView iv, TextView tv, String talentName) {
		ImageLoader.getInstance().displayImage(imgUrl, iv);
		tv.setText(talentName);
	}

	private void initViews(View rootView) {
		ivTier1Image1 = (ImageView) rootView.findViewById(R.id.talentIvTier1Image1);
		ivTier1Image2 = (ImageView) rootView.findViewById(R.id.talentIvTier1Image2);
		ivTier1Image3 = (ImageView) rootView.findViewById(R.id.talentIvTier1Image3);
		ivTier2Image1 = (ImageView) rootView.findViewById(R.id.talentIvTier2Image1);
		ivTier2Image2 = (ImageView) rootView.findViewById(R.id.talentIvTier2Image2);
		ivTier2Image3 = (ImageView) rootView.findViewById(R.id.talentIvTier2Image3);
		ivTier3Image1 = (ImageView) rootView.findViewById(R.id.talentIvTier3Image1);
		ivTier3Image2 = (ImageView) rootView.findViewById(R.id.talentIvTier3Image2);
		ivTier3Image3 = (ImageView) rootView.findViewById(R.id.talentIvTier3Image3);
		ivTier4Image1 = (ImageView) rootView.findViewById(R.id.talentIvTier4Image1);
		ivTier4Image2 = (ImageView) rootView.findViewById(R.id.talentIvTier4Image2);
		ivTier4Image3 = (ImageView) rootView.findViewById(R.id.talentIvTier4Image3);
		ivTier5Image1 = (ImageView) rootView.findViewById(R.id.talentIvTier5Image1);
		ivTier5Image2 = (ImageView) rootView.findViewById(R.id.talentIvTier5Image2);
		ivTier5Image3 = (ImageView) rootView.findViewById(R.id.talentIvTier5Image3);
		ivTier6Image1 = (ImageView) rootView.findViewById(R.id.talentIvTier6Image1);
		ivTier6Image2 = (ImageView) rootView.findViewById(R.id.talentIvTier6Image2);
		ivTier6Image3 = (ImageView) rootView.findViewById(R.id.talentIvTier6Image3);
		tvTier1Talent1 = (TextView) rootView.findViewById(R.id.talentTvTier1Text1);
		tvTier1Talent2 = (TextView) rootView.findViewById(R.id.talentTvTier1Text2);
		tvTier1Talent3 = (TextView) rootView.findViewById(R.id.talentTvTier1Text3);
		tvTier2Talent1 = (TextView) rootView.findViewById(R.id.talentTvTier2Text1);
		tvTier2Talent2 = (TextView) rootView.findViewById(R.id.talentTvTier2Text2);
		tvTier2Talent3 = (TextView) rootView.findViewById(R.id.talentTvTier2Text3);
		tvTier3Talent1 = (TextView) rootView.findViewById(R.id.talentTvTier3Text1);
		tvTier3Talent2 = (TextView) rootView.findViewById(R.id.talentTvTier3Text2);
		tvTier3Talent3 = (TextView) rootView.findViewById(R.id.talentTvTier3Text3);
		tvTier4Talent1 = (TextView) rootView.findViewById(R.id.talentTvTier4Text1);
		tvTier4Talent2 = (TextView) rootView.findViewById(R.id.talentTvTier4Text2);
		tvTier4Talent3 = (TextView) rootView.findViewById(R.id.talentTvTier4Text3);
		tvTier5Talent1 = (TextView) rootView.findViewById(R.id.talentTvTier5Text1);
		tvTier5Talent2 = (TextView) rootView.findViewById(R.id.talentTvTier5Text2);
		tvTier5Talent3 = (TextView) rootView.findViewById(R.id.talentTvTier5Text3);
		tvTier6Talent1 = (TextView) rootView.findViewById(R.id.talentTvTier6Text1);
		tvTier6Talent2 = (TextView) rootView.findViewById(R.id.talentTvTier6Text2);
		tvTier6Talent3 = (TextView) rootView.findViewById(R.id.talentTvTier6Text3);
	}
}