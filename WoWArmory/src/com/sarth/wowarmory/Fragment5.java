package com.sarth.wowarmory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Fragment5 extends Fragment implements JSONParserCallback {

	private RelativeLayout rrSecondaryFirstAid, rrSecondaryFishing;
	private TextView tvPrimaryProfName1, tvPrimaryProfLevel1, tvPrimaryProfRank1;
	private TextView tvPrimaryProfName2, tvPrimaryProfLevel2, tvPrimaryProfRank2;
	private ImageView ivPrimaryProf1, ivPrimaryProf2;
	private JSONGet taskGet;
	private View v;

	public Fragment5() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment5, container, false);
		v = rootView;
		rrSecondaryFirstAid = (RelativeLayout) rootView.findViewById(R.id.rrSecondaryFirstAid);
		rrSecondaryFishing = (RelativeLayout) rootView.findViewById(R.id.rrSecondaryFishing);
		tvPrimaryProfName1 = (TextView) rootView.findViewById(R.id.tvPrimaryProfName1);
		tvPrimaryProfName2 = (TextView) rootView.findViewById(R.id.tvPrimaryProfName2);
		tvPrimaryProfRank1 = (TextView) rootView.findViewById(R.id.tvPrimaryProfLevel1);
		tvPrimaryProfRank2 = (TextView) rootView.findViewById(R.id.tvPrimaryProfLevel2);
		tvPrimaryProfLevel1 = (TextView) rootView.findViewById(R.id.pbPrimaryProf1);
		tvPrimaryProfLevel2 = (TextView) rootView.findViewById(R.id.pbPrimaryProf2);
		ivPrimaryProf1 = (ImageView) rootView.findViewById(R.id.ivPrimaryProf1);
		ivPrimaryProf2 = (ImageView) rootView.findViewById(R.id.ivPrimaryProf2);
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (menuVisible) {
			if (!Commons.INST.fragment5done) {
				String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
						+ "?fields=professions";
				taskGet = new JSONGet(this, adres);
				taskGet.execute();
			}

		} else {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		if (jsonGet != null) {
			try {
				/** Primary Professions */
				if (checkForPrimaryProfessions(jsonGet)) {
					getPrimaryProfessions(jsonGet);
				}
				/** Secondary Professions */
				if (checkForSecondaryProf(jsonGet, 0)) {
					TextView tvSecondaryRank = (TextView) v.findViewById(R.id.tvSecondaryProfLevel3);
					TextView tvSecondaryLevel = (TextView) v.findViewById(R.id.pbSecondaryProf3);
					getSecondaryProf(jsonGet, 0, tvSecondaryRank, tvSecondaryLevel);
				}
				if (checkForSecondaryProf(jsonGet, 1)) {
					TextView tvSecondaryRank = (TextView) v.findViewById(R.id.tvSecondaryProfLevel1);
					TextView tvSecondaryLevel = (TextView) v.findViewById(R.id.pbSecondaryProf1);
					getSecondaryProf(jsonGet, 1, tvSecondaryRank, tvSecondaryLevel);
				}
				if (checkForSecondaryProf(jsonGet, 2)) {
					TextView tvSecondaryRank = (TextView) v.findViewById(R.id.tvSecondaryProfLevel4);
					TextView tvSecondaryLevel = (TextView) v.findViewById(R.id.pbSecondaryProf4);
					getSecondaryProf(jsonGet, 2, tvSecondaryRank, tvSecondaryLevel);
				}
				if (checkForSecondaryProf(jsonGet, 3)) {
					TextView tvSecondaryRank = (TextView) v.findViewById(R.id.tvSecondaryProfLevel2);
					TextView tvSecondaryLevel = (TextView) v.findViewById(R.id.pbSecondaryProf2);
					getSecondaryProf(jsonGet, 3, tvSecondaryRank, tvSecondaryLevel);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private void getSecondaryProf(JSONObject jsonGet, int i, TextView tvSecondaryRank, TextView tvSecondaryLevel) throws JSONException {
		int a = jsonGet.getJSONObject("professions").getJSONArray("secondary").getJSONObject(i).getInt("rank");
		int b = jsonGet.getJSONObject("professions").getJSONArray("secondary").getJSONObject(i).getInt("max");
		tvSecondaryRank.setText(getProfRank(a));
		tvSecondaryLevel.setText(a + "/" + b);
	}

	private boolean checkForPrimaryProfessions(JSONObject jsonGet) throws JSONException {
		JSONArray j = jsonGet.getJSONObject("professions").getJSONArray("primary");
		if (j.length() != 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkForSecondaryProf(JSONObject jsonGet, int i) throws JSONException {
		int max = jsonGet.getJSONObject("professions").getJSONArray("secondary").getJSONObject(i).getInt("max");
		if (max == 0) {
			return false;
		} else {
			return true;
		}
	}

	private void getPrimaryProfessions(JSONObject jsonGet) throws JSONException {
		JSONArray j = jsonGet.getJSONObject("professions").getJSONArray("primary");
		tvPrimaryProfName1.setText(j.getJSONObject(0).getString("name"));
		int a = j.getJSONObject(0).getInt("rank");
		int b = j.getJSONObject(0).getInt("max");
		tvPrimaryProfLevel1.setText(a + "/" + b);
		tvPrimaryProfRank1.setText(getProfRank(a));
		String icon = j.getJSONObject(0).getString("icon");
		String adres = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/" + icon + ".jpg";
		ImageLoader.getInstance().displayImage(adres, ivPrimaryProf1);
		if (j.length() == 2) {
			tvPrimaryProfName2.setText(j.getJSONObject(1).getString("name"));
			int aa = j.getJSONObject(1).getInt("rank");
			int bb = j.getJSONObject(1).getInt("max");
			tvPrimaryProfLevel2.setText(aa + "/" + bb);
			tvPrimaryProfRank2.setText(getProfRank(aa));
			String icon1 = j.getJSONObject(1).getString("icon");
			String adres1 = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/" + icon1 + ".jpg";
			ImageLoader.getInstance().displayImage(adres1, ivPrimaryProf2);
		}
		Commons.INST.fragment5done = true;
	}

	private String getProfRank(int a) {
		if (a <= 75) {
			return "Apprentice";
		} else if (a > 75 && a <= 150) {
			return "Journeyman";
		} else if (a > 150 && a <= 225) {
			return "Expert";
		} else if (a > 225 && a <= 300) {
			return "Artisan";
		} else if (a > 300 && a <= 375) {
			return "Master";
		} else if (a > 375 && a <= 450) {
			return "Grand Master";
		} else if (a > 450 && a <= 525) {
			return "Illustrious Grand Master";
		} else if (a > 525 && a <= 600) {
			return "Zen Master";
		}
		return null;
	}
}