package com.sarth.wowarmory;

import com.sarth.wowarmory.adapters.FragmentTabsAdapter;
import com.sarth.wowarmory.interfaces.BackButtonCallback;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

public class CharActivity extends FragmentActivity {

	private FragmentTabsAdapter tabsAdapter;
	private BackButtonCallback back;
	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (Commons.INST.realmSlug == null) {
			startActivity(new Intent(this, SplashScreen.class));
		} else {
			setContentView(R.layout.activity_char);
			tabsAdapter = new FragmentTabsAdapter(getSupportFragmentManager());
			mViewPager = (ViewPager) findViewById(R.id.pager);
			mViewPager.setOffscreenPageLimit(15);
			mViewPager.setAdapter(tabsAdapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (getSupportFragmentManager().getFragments().get(3).isMenuVisible()) {
			back = (BackButtonCallback) getSupportFragmentManager().getFragments().get(3);
			back.onBackButtonPressed();
		}
	}
}