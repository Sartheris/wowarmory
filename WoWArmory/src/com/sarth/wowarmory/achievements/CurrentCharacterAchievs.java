package com.sarth.wowarmory.achievements;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class CurrentCharacterAchievs implements Parcelable {

	public String achivId, achivDate;
	public long Id;

	public ArrayList<CurrentCharacterAchievs> fromJson(JSONObject jsonGet) throws JSONException, OutOfMemoryError {
		ArrayList<CurrentCharacterAchievs> ccaArray = new ArrayList<CurrentCharacterAchievs>();
		CurrentCharacterAchievs cca;
		JSONArray jj = new JSONArray();
		JSONArray jjDate = new JSONArray();
		jj = jsonGet.getJSONObject("achievements").getJSONArray("achievementsCompleted");
		jjDate = jsonGet.getJSONObject("achievements").getJSONArray("achievementsCompletedTimestamp");
		for (int i = 0; i < jj.length(); i++) {
			cca = new CurrentCharacterAchievs();
			cca.achivId = jj.getString(i);
			cca.achivDate = jjDate.getString(i);
			ccaArray.add(cca);
		}
		return ccaArray;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(achivId);
		dest.writeString(achivDate);
		dest.writeLong(Id);

	}

	public static final Parcelable.Creator<CurrentCharacterAchievs> CREATOR = new Parcelable.Creator<CurrentCharacterAchievs>() {

		@Override
		public CurrentCharacterAchievs createFromParcel(Parcel source) {
			CurrentCharacterAchievs curr = new CurrentCharacterAchievs();
			curr.achivId = source.readString();
			curr.achivDate = source.readString();
			curr.Id = source.readLong();
			return curr;
		}

		@Override
		public CurrentCharacterAchievs[] newArray(int size) {
			return new CurrentCharacterAchievs[size];
		}
	};
}