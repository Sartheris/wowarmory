package com.sarth.wowarmory.achievements;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AchievementsParser {

	public ArrayList<AchievementCategory> categories;

	public ArrayList<AchievementCategory> parseAll(JSONObject object) {
		try {
			JSONArray arr = object.getJSONArray("achievements");
			categories = new ArrayList<AchievementCategory>();
			for (int i = 0; i < arr.length(); i++) {
				AchievementCategory ac = parseCategory(arr.getJSONObject(i));
				categories.add(ac);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return categories;
	}

	private AchievementCategory parseCategory(JSONObject obj) throws JSONException {
		AchievementCategory ac = new AchievementCategory();
		ac.CategoryId = obj.getInt("id");
		ac.CategoryName = obj.getString("name");
		ac.Achievements = parseAchievement(obj.getJSONArray("achievements"));
		if (obj.has("categories")) {
			ArrayList<AchievementCategory> subCategories = new ArrayList<AchievementCategory>();
			JSONArray subs = obj.getJSONArray("categories");
			for (int j = 0; j < subs.length(); j++) {
				AchievementCategory cat = parseCategory(subs.getJSONObject(j));
				subCategories.add(cat);
			}
			ac.SubCategories = subCategories;
		}
		return ac;
	}

	private ArrayList<Achievement> parseAchievement(JSONArray obj) throws JSONException {
		ArrayList<Achievement> aaArray = new ArrayList<Achievement>();
		for (int j = 0; j < obj.length(); j++) {
			JSONObject jj = obj.getJSONObject(j);
			Achievement ach = new Achievement();
			ach.achivId = jj.getInt("id");
			ach.achivIcon = jj.getString("icon");
			ach.achivTitle = jj.getString("title");
			ach.achivPoints = jj.getInt("points");
			ach.achivDescription = jj.getString("description");
			aaArray.add(ach);
		}
		return aaArray;
	}
}