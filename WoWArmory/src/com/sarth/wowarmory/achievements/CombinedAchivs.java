package com.sarth.wowarmory.achievements;

public class CombinedAchivs {

	public int typeId;
	public Achievement achiv;
	public AchievementCategory achivCategory;

	public CombinedAchivs(int typeId, Achievement achiv, AchievementCategory achivCategory) {
		this.typeId = typeId;
		this.achiv = achiv;
		this.achivCategory = achivCategory;
	}
}