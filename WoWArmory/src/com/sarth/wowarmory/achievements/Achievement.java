package com.sarth.wowarmory.achievements;

import android.os.Parcel;
import android.os.Parcelable;

public class Achievement implements Parcelable {

	public Long id;
	public int achivId, achivPoints, achivCategoryId;
	public String achivTitle, achivDescription, achivIcon, achivCategory;
	public String hasAchiv, achivDate;

	public Achievement() {
		this.id = 0l;
		this.achivTitle = "";
		this.achivDescription = "";
		this.achivIcon = "";
		this.achivId = 0;
		this.achivPoints = 0;
	}

	public Achievement(Long id2, int achivId2, String achivIcon2, String achivName) {
		this.id = id2;
		this.achivId = achivId2;
		this.achivIcon = achivIcon2;
		this.achivTitle = achivName;
	}

	public Achievement(String s, String icon, String description, int points, String hasAchiv, String achivDate) {
		this.achivTitle = s;
		this.achivIcon = icon;
		this.achivDescription = description;
		this.achivPoints = points;
		this.hasAchiv = hasAchiv;
		this.achivDate = achivDate;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(achivId);
		dest.writeInt(achivPoints);
		dest.writeInt(achivCategoryId);
		dest.writeString(achivTitle);
		dest.writeString(achivDescription);
		dest.writeString(achivIcon);
		dest.writeString(achivCategory);
	}

	public static final Parcelable.Creator<Achievement> CREATOR = new Parcelable.Creator<Achievement>() {

		@Override
		public Achievement createFromParcel(Parcel source) {
			Achievement achiv = new Achievement();
			achiv.achivId = source.readInt();
			achiv.achivPoints = source.readInt();
			achiv.achivCategoryId = source.readInt();
			achiv.achivTitle = source.readString();
			achiv.achivDescription = source.readString();
			achiv.achivIcon = source.readString();
			achiv.achivCategory = source.readString();
			return achiv;
		}

		@Override
		public Achievement[] newArray(int size) {
			return new Achievement[size];
		}
	};
}