package com.sarth.wowarmory.achievements;

import java.util.ArrayList;

public class AchievementCategory {

	public int CategoryId, ParentId;
	public long Id;
	public String CategoryName;
	public ArrayList<Achievement> Achievements;
	public ArrayList<AchievementCategory> SubCategories;

	public AchievementCategory() {
		// Empty constructor
	}

	public AchievementCategory(String name, long id, int categoryId, int parentId) {
		this.CategoryName = name;
		this.Id = id;
		this.CategoryId = categoryId;
		this.ParentId = parentId;
	}
}