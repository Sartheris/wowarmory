package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONObject;
import com.sarth.wowarmory.achievements.AchievementCategory;
import com.sarth.wowarmory.achievements.AchievementsParser;
import com.sarth.wowarmory.asynctasks.AchivsLoadingTask;
import com.sarth.wowarmory.asynctasks.CurrentCharLoadingTask;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.BackButtonCallback;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.sqlite.DataHelper;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Fragment4 extends Fragment implements JSONParserCallback, BackButtonCallback {

	private LoadingDialogFragment loadingFragment;
	private Button butReloadAchivs;
	private TextView tvReloadAchivs;
	private boolean allAchivsLoaded;
	private SharedPreferences sharedPref;
	private AchivsLoadingTask achivLoad;
	private JSONGet taskGet;
	private DataHelper dh;
	private LinearLayout ll;
	private int operationId;

	public Fragment4() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment4, container, false);
		ll = (LinearLayout) rootView.findViewById(R.id.fragment4layout);
		dh = new DataHelper(getActivity());
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (menuVisible) {
			sharedPref = getActivity().getSharedPreferences("realm", Context.MODE_PRIVATE);
			allAchivsLoaded = sharedPref.getBoolean("achivs", false);
			if (allAchivsLoaded && Commons.INST.fragment4done) {
				createAchievementsViews();
			} else if (!allAchivsLoaded) {
				createButtonForRefresh();
			} else if (allAchivsLoaded && !Commons.INST.fragment4done) {
				createAchievementsViews();
			}
		} else {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
			if (achivLoad != null) {
				achivLoad.cancel(true);
			}
		}
	}

	private void startParsingAllAchivs() {
		operationId = 1;
		String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/data/character/achievements";
		loadingFragment = LoadingDialogFragment.newInstance(getString(R.string.loading_all_achivs));
		loadingFragment.show(getActivity().getSupportFragmentManager(), "tag");
		taskGet = new JSONGet(this, adres);
		taskGet.execute();
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		switch (operationId) {
		case 1:
			AchievementsParser ap = new AchievementsParser();
			ArrayList<AchievementCategory> cats = ap.parseAll(jsonGet);
			operationId = 3;
			achivLoad = new AchivsLoadingTask(this, getActivity(), cats);
			achivLoad.execute();
			break;
		case 2:
			operationId = 4;
			CurrentCharLoadingTask cc = new CurrentCharLoadingTask(this, getActivity(), jsonGet);
			cc.execute();
			break;
		case 3:
			SharedPreferences.Editor prefEditor = sharedPref.edit();
			prefEditor.putBoolean("achivs", true);
			prefEditor.commit();
			String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
					+ "?fields=achievements";
			taskGet = new JSONGet(this, adres);
			taskGet.execute();
			operationId = 2;
			break;
		case 4:
			Commons.INST.charAchivsNeedUpdating = false;
			createAchievementsViews();
			break;
		}
	}

	private void createAchievementsViews() {
		ArrayList<AchievementCategory> arrAC = dh.selectCategories(0);
		for (int i = 0; i < arrAC.size(); i++) {
			final Button b = new Button(getActivity());
			b.setText(arrAC.get(i).CategoryName);
			b.setTag(arrAC.get(i).Id);
			b.setTextColor(getActivity().getResources().getColor(R.color.common));
			b.setBackgroundResource(R.drawable.achiv);
			if (butReloadAchivs != null && tvReloadAchivs != null) {
				butReloadAchivs.setVisibility(View.GONE);
				tvReloadAchivs.setVisibility(View.GONE);
			}
			ll.addView(b);
			b.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					long tag = (Long) v.getTag();
					Intent i = new Intent(getActivity(), AchievementSubCategories.class);
					i.putExtra("parentId", tag);
					startActivity(i);
				}

			});
			b.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						b.setAlpha((float) 0.8);
						b.invalidate();
						break;
					case MotionEvent.ACTION_UP:
						b.setAlpha((float) 1.0);
						b.invalidate();
						break;
					case MotionEvent.ACTION_CANCEL:
						b.setAlpha((float) 1.0);
						b.invalidate();
					default:
						break;
					}
					return false;
				}
			});
			if (loadingFragment != null) {
				loadingFragment.dismiss();
			}
		}
		Commons.INST.fragment4done = true;
	}

	@Override
	public void onBackButtonPressed() {
		if (loadingFragment != null) {
			loadingFragment.dismiss();
		}
		if (taskGet != null) {
			taskGet.cancel(true);
		}
		createButtonForRefresh();
	}

	private void createButtonForRefresh() {
		butReloadAchivs = new Button(getActivity());
		butReloadAchivs.setText("Load Achievements");
		butReloadAchivs.setBackgroundResource(R.color.common);
		butReloadAchivs.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// ot tuka da se startira nanovo parsinga
				startParsingAllAchivs();
			}
		});
		tvReloadAchivs = new TextView(getActivity());
		tvReloadAchivs.setText("Achievements have not been loaded, please refresh and wait a while...");
		tvReloadAchivs.setBackgroundResource(R.color.common);
		ll.addView(butReloadAchivs);
		ll.addView(tvReloadAchivs);
	}
}