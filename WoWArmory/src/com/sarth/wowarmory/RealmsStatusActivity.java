package com.sarth.wowarmory;

import java.util.ArrayList;
import com.sarth.wowarmory.adapters.RealmsStatusListAdapter;
import com.sarth.wowarmory.models.RealmsInfo;
import android.os.Bundle;
import android.widget.ListView;
import android.app.Activity;

public class RealmsStatusActivity extends Activity {

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_realms_status);
		if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().get("realmi") != null) {
			ArrayList<RealmsInfo> ri = (ArrayList<RealmsInfo>) getIntent().getExtras().get("realmi");
			ListView lv = (ListView) findViewById(R.id.listViewRealmsStatus);
			RealmsStatusListAdapter adap = new RealmsStatusListAdapter(this, android.R.layout.simple_spinner_dropdown_item, ri, 1);
			lv.setAdapter(adap);
		}
	}
}