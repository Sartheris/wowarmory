package com.sarth.wowarmory.interfaces;

import org.json.JSONObject;

public interface JSONParserCallback {
	
	void onFinishedParsing(JSONObject jsonGet);
}