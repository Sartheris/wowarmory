package com.sarth.wowarmory.interfaces;

public interface BackButtonCallback {
	
	void onBackButtonPressed();
}