package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.adapters.PetsListAdapter;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.mountsandpets.Pets;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class Fragment12 extends Fragment implements JSONParserCallback {

	private LoadingDialogFragment loadingFragment;
	private TextView tvCollected, tvNotCollected;
	private JSONGet taskGet;
	private ListView lv;

	public Fragment12() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment12, container, false);
		lv = (ListView) rootView.findViewById(R.id.lvPets);
		tvCollected = (TextView) rootView.findViewById(R.id.tvPetsCollected);
		tvNotCollected = (TextView) rootView.findViewById(R.id.tvPetsNotCollected);
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (!menuVisible) {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
			if (loadingFragment != null) {
				loadingFragment.dismiss();
			}
		} else {
			if (!Commons.INST.fragment12done) {
				loadingFragment = LoadingDialogFragment.newInstance("Getting character pets...");
				loadingFragment.show(getActivity().getSupportFragmentManager(), "tag");
				String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
						+ "?fields=pets" + "&locale=" + Commons.INST.locale;
				taskGet = new JSONGet(this, adres);
				taskGet.execute();
			}
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		try {
			tvCollected.setText("Pets collected: " + jsonGet.getJSONObject("pets").getString("numCollected"));
			tvNotCollected.setText("Pets not collected: " + jsonGet.getJSONObject("pets").getString("numNotCollected"));
			JSONArray jArr = jsonGet.getJSONObject("pets").getJSONArray("collected");
			ArrayList<Object> arr = new ArrayList<Object>();
			for (int i = 0; i < jArr.length(); i++) {
				JSONObject j = jArr.getJSONObject(i);
				Pets p = new Pets();
				p.petName = j.getString("name");
				p.petIcon = j.getString("icon");
				p.petQuality = j.getInt("qualityId");
				arr.add(p);
			}
			PetsListAdapter adap = new PetsListAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, arr);
			lv.setAdapter(adap);
			loadingFragment.dismiss();
			Commons.INST.fragment12done = true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}