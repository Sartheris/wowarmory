package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.adapters.ReputationListAdapter;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.reputation.Reputation;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class Fragment9 extends Fragment implements JSONParserCallback {

	private LoadingDialogFragment loadingFragment;
	private JSONGet taskGet;
	private ListView lv;

	public Fragment9() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment9, container, false);
		lv = (ListView) rootView.findViewById(R.id.lvReputation);
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (menuVisible && !Commons.INST.fragment9done) {
			loadingFragment = LoadingDialogFragment.newInstance("Getting character reputation...");
			loadingFragment.show(getActivity().getSupportFragmentManager(), "tag");
			String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
					+ "?fields=reputation" + "&locale=" + Commons.INST.locale;
			taskGet = new JSONGet(this, adres);
			taskGet.execute();
		} else {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
			if (loadingFragment != null) {
				loadingFragment.dismiss();
			}
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		try {
			ArrayList<Object> arr = new ArrayList<Object>();
			JSONArray jArr = jsonGet.getJSONArray("reputation");
			for (int i = 0; i < jArr.length(); i++) {
				JSONObject j = jArr.getJSONObject(i);
				Reputation rep = new Reputation();
				rep.name = j.getString("name");
				rep.standing = j.getInt("standing");
				rep.value = j.getInt("value");
				rep.max = j.getInt("max");
				arr.add(rep);
			}
			ReputationListAdapter adap = new ReputationListAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, arr);
			lv.setAdapter(adap);
			Commons.INST.fragment9done = true;
			loadingFragment.dismiss();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}