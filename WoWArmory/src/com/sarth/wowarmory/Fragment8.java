package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONObject;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.feed.LooperClass;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class Fragment8 extends Fragment implements JSONParserCallback {

	private LoadingDialogFragment loadingFragment;
	private JSONGet taskGet;
	private ListView lv;

	public Fragment8() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment8, container, false);
		lv = (ListView) rootView.findViewById(R.id.lvFeed);
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (menuVisible && !Commons.INST.fragment8done) {
			loadingFragment = LoadingDialogFragment.newInstance("Getting character recent activity feed...");
			loadingFragment.show(getActivity().getSupportFragmentManager(), "tag");
			String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
					+ "?fields=feed" + "&locale=" + Commons.INST.locale;
			taskGet = new JSONGet(this, adres);
			taskGet.execute();
		} else {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
			if (loadingFragment != null) {
				loadingFragment.dismiss();
			}
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		ArrayList<Object> feedArray = new ArrayList<Object>();
		LooperClass loop = new LooperClass(jsonGet, feedArray, getActivity(), lv, loadingFragment);
		loop.arrayListLoop(feedArray, getActivity(), lv, jsonGet);
	}
}