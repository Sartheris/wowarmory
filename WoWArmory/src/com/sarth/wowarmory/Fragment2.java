package com.sarth.wowarmory;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.models.GearItem;
import com.sarth.wowarmory.popupcontents.CheckForGems;
import com.sarth.wowarmory.popupcontents.GetItemStats;
import com.sarth.wowarmory.popupcontents.SellPrice;

public class Fragment2 extends Fragment implements JSONParserCallback {

	private HashMap<String, GearItem> gear;
	private JSONGet taskGet;
	private ImageView iv1, iv2, iv3, iv4, iv5, iv6, iv7, iv8, iv9, iv10;
	private ImageView iv11, iv12, iv13, iv14, iv15, iv16, iv17, iv18, ivP;
	private TextView tvCharClass, tvCharLevel, tvCharName, tvCharRace;
	private View baseView;
	private int operationID;
	private PopupWindow popup;

	public Fragment2() {
		// Empty constructor for Fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment2, container, false);
		initViews(rootView);
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (!menuVisible) {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
			if (popup != null) {
				popup.dismiss();
			}
		} else {
			if (!Commons.INST.fragment2done) {
				String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/"
						+ Commons.INST.charName + "?fields=items" + "&locale=" + Commons.INST.locale;
				operationID = 5;
				startJson(adres);
			}
		}
	}

	private void startJson(String adres) {
		taskGet = new JSONGet(this, adres);
		taskGet.execute();
	}

	private void createImageViews() {
		tvCharName.setText(Commons.INST.charName);
		tvCharClass.setText(Commons.INST.charClass);
		tvCharRace.setText(Commons.INST.charRace);
		tvCharLevel.setText("Level " + Commons.INST.charLevel);
		String thumbId = Commons.INST.charThumbnail.replace(Commons.INST.realmSlug, "").replace("-avatar.jpg", "");
		final String imgUrl = "http://" + Commons.INST.regionUrl + ".battle.net/static-render/" + Commons.INST.regionUrl + "/"
				+ Commons.INST.realmSlug + thumbId + "-profilemain.jpg";
		ImageLoader.getInstance().displayImage(imgUrl, ivP);
		ivP.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), ShowImage.class);
				i.putExtra("adres", imgUrl);
				startActivity(i);

			}
		});
		loadItem("head", iv1);
		loadItem("neck", iv2);
		loadItem("shoulder", iv3);
		loadItem("back", iv4);
		loadItem("chest", iv5);
		loadItem("shirt", iv6);
		loadItem("tabard", iv7);
		loadItem("wrist", iv8);
		loadItem("hands", iv9);
		loadItem("waist", iv10);
		loadItem("legs", iv11);
		loadItem("feet", iv12);
		loadItem("finger1", iv13);
		loadItem("finger2", iv14);
		loadItem("trinket1", iv15);
		loadItem("trinket2", iv16);
		loadItem("mainHand", iv17);
		loadItem("trinket2", iv16);
		loadItem("offHand", iv18);
		Commons.INST.fragment2done = true;
	}

	private void loadItem(final String item, ImageView ivItem) {
		if (gear.get(item) != null && gear.get(item).icon != null) {
			initializeImageView(ivItem, item);
			ivItem.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (gear.get(item) != null) {
						baseView = v;
						Commons.INST.currentItem = item;
						setViewsEnabled(false);
						operationID = 1;
						showPopup(v);
						String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/item/" + gear.get(item).itemId;
						startJson(adres);
					}
				}
			});
		}
	}

	private void initializeImageView(ImageView iv, String s) {
		String imgUrl = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/" + gear.get(s).icon + ".jpg";
		ImageLoader.getInstance().displayImage(imgUrl, iv);
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		switch (operationID) {
		case 1:
			ArrayList<GearItem> arrList = new ArrayList<GearItem>();
			if (jsonGet != null) {
				arrList.add(GearItem.singleItemFromJson(jsonGet));
			}
			/** Check for sockets */
			if (gear.get(Commons.INST.currentItem).hasSockets == true) {
				operationID = 2;
				if (gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Id != 0) {
					int gemId = gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Id;
					String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/item/" + gemId;
					startJson(adres);
				} else {
					onFinishedParsing(jsonGet);
				}
			} else {
				/** No sockets */
				showPopupContents(baseView);
			}
			break;
		case 2:
			operationID = 3;
			try {
				if (gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Id != 0) {
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem0 = jsonGet.getJSONObject("gemInfo").getJSONObject("bonus")
							.getString("name");
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Type = jsonGet.getJSONObject("gemInfo").getJSONObject("type")
							.getString("type");
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Icon = jsonGet.getString("icon");
				}
				if (jsonGet.has("gemInfo") && gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Id != 0) {
					int gemId = gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Id;
					String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/item/" + gemId;
					startJson(adres);
				} else {
					onFinishedParsing(jsonGet);
				}
			} catch (JSONException e4) {
				e4.printStackTrace();
			}
			break;
		case 3:
			operationID = 4;
			try {
				if (gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Id != 0) {
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem1 = jsonGet.getJSONObject("gemInfo").getJSONObject("bonus")
							.getString("name");
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Type = jsonGet.getJSONObject("gemInfo").getJSONObject("type")
							.getString("type");
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Icon = jsonGet.getString("icon");
				}
				if (jsonGet.has("gemInfo") && gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Id != 0) {
					int gemId = gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Id;
					String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/item/" + gemId;
					startJson(adres);
				} else {
					onFinishedParsing(jsonGet);
				}
			} catch (JSONException e3) {
				e3.printStackTrace();
			}
			break;
		case 4:
			try {
				if (gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Id != 0) {
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem2 = jsonGet.getJSONObject("gemInfo").getJSONObject("bonus")
							.getString("name");
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Type = jsonGet.getJSONObject("gemInfo").getJSONObject("type")
							.getString("type");
					gear.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Icon = jsonGet.getString("icon");
				}
				showPopupContents(baseView);
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			break;
		case 5:
			if (jsonGet != null) {
				ArrayList<GearItem> gearItem = new ArrayList<GearItem>();
				gearItem.add(GearItem.getAllEquippedItems(jsonGet));
				gear = Commons.currentCharacter.gearItems;
				createImageViews();
			}
			break;
		default:
			break;
		}
	}

	/** Show pop up window */
	public void showPopup(View anchorView) {
		LayoutInflater li = (LayoutInflater) getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = li.inflate(R.layout.popup, null);
		baseView = popupView;
		popup = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popup.setBackgroundDrawable(new ColorDrawable());
		popup.setFocusable(true);
		int location[] = new int[2];
		anchorView.getLocationOnScreen(location);
		popup.showAtLocation(anchorView, Gravity.NO_GRAVITY, location[0], location[1] + anchorView.getHeight());
		TextView tvItemName = (TextView) popupView.findViewById(R.id.tvpopItemName);
		tvItemName.setTextColor(getResources().getColor(R.color.common));
		tvItemName.setText("Loading item...");
		popupView.setOnTouchListener(new OnTouchListener() {
			/** Popup Window Drag Event */
			int dx = 0;
			int dy = 0;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					dx = (int) event.getX();
					dy = (int) event.getY();
					break;
				case MotionEvent.ACTION_MOVE:
					int x = (int) event.getRawX();
					int y = (int) event.getRawY();
					int left = (x - dx);
					int top = (y - dy);
					popup.update(left, top, -1, -1);
					break;
				case MotionEvent.ACTION_CANCEL:
					popup.dismiss();
					break;
				}
				return true;
			}
		});
	}

	private void showPopupContents(View popupView) {
		/** Initialize popup text views */
		String item = Commons.INST.currentItem;
		popupView = baseView;
		LinearLayout llDurability = (LinearLayout) popupView.findViewById(R.id.tvpopDurability);
		TextView tv;
		/** Item Name */
		TextView tvItemName = (TextView) popupView.findViewById(R.id.tvpopItemName);
		tvItemName.setText(gear.get(item).name);
		tvItemName.setTextColor(getResources().getColor(Commons.getItemColor((gear.get(item).quality))));
		/** Item Description */
		if (!gear.get(item).nameDescription.equals("")) {
			TextView tvDesc = (TextView) popupView.findViewById(R.id.tvpopItemNameDescription);
			tvDesc.setText(gear.get(item).nameDescription + "");
			tvDesc.setVisibility(View.VISIBLE);
		}
		/** Item Level */
		TextView tvItemLevel = (TextView) popupView.findViewById(R.id.tvpopItemLevel);
		tvItemLevel.setText("Item Level " + gear.get(item).itemLevel);
		tvItemLevel.setTextColor(getResources().getColor(R.color.itemlevel));
		TextView tvItemType = (TextView) popupView.findViewById(R.id.tvpopArmorClass);
		if (gear.get(item).itemClass == 2) {
			tvItemType.setText(Commons.getItemType(gear.get(item).itemSubClass));
		}
		if (gear.get(item).itemClass == 4) {
			tvItemType.setText(Commons.getItemSubclass(gear.get(item).itemClass));
		}
		/** Bind */
		TextView tvBind = (TextView) popupView.findViewById(R.id.tvpopBind);
		TextView tvSlot = (TextView) popupView.findViewById(R.id.tvpopSlot);
		tvSlot.setText(Commons.getInventoryType(gear.get(item).inventoryType));
		switch (gear.get(item).itemBind) {
		case 1:
			tvBind.setText("Binds when picked up");
			tvBind.setVisibility(View.VISIBLE);
			break;
		case 2:
			tvBind.setText("Binds on equip");
			tvBind.setVisibility(View.VISIBLE);
			break;
		default:
			break;
		}
		/** Weapon Damage Info */
		if (gear.get(item).wepInfo != null && gear.get(item).wepInfo.wepDps > 0.0) {
			TextView tvWepDmg = (TextView) popupView.findViewById(R.id.tvpopWepDmg);
			TextView tvWepDps = (TextView) popupView.findViewById(R.id.tvpopWepDps);
			TextView tvWepSpeed = (TextView) popupView.findViewById(R.id.tvpopWepSpeed);
			int minDmg = gear.get(item).wepInfo.wepMinDmg;
			int maxDmg = gear.get(item).wepInfo.wepMaxDmg;
			int dps = ((int) gear.get(item).wepInfo.wepDps);
			double speed = gear.get(item).wepInfo.wepSpeed;
			tvWepDmg.setText(minDmg + "-" + maxDmg + " Damage");
			tvWepDps.setText("(" + dps + " damage per second)");
			tvWepSpeed.setText("Speed " + speed);
			tvWepDmg.setVisibility(View.VISIBLE);
			tvWepDps.setVisibility(View.VISIBLE);
			tvWepSpeed.setVisibility(View.VISIBLE);
		}
		/** Armor */
		if (gear.get(item).armor != 0) {
			TextView tvArmor = (TextView) popupView.findViewById(R.id.tvpopArmor);
			tvArmor.setText(gear.get(item).armor + " Armor");
			tvArmor.setVisibility(View.VISIBLE);
		}
		/** Upgrade Level */
		if (gear.get(item).tooltipParamsInfo != null && gear.get(item).tooltipParamsInfo.iui != null
				&& gear.get(item).tooltipParamsInfo.iui.total > 0) {
			TextView tvItemUpgradeCurrent = (TextView) popupView.findViewById(R.id.tvpopUpgradable);
			tvItemUpgradeCurrent.setText("Upgrade Level " + gear.get(item).tooltipParamsInfo.iui.current + "/"
					+ gear.get(item).tooltipParamsInfo.iui.total);
			tvItemUpgradeCurrent.setVisibility(View.VISIBLE);
		}
		/** Item Stats */
		if (gear.get(item).itemStat != null && gear.get(item).itemStat.size() > 0) {
			GetItemStats stats = new GetItemStats(popupView, getActivity());
			stats.getStats();
		} else {
			/** No item stats available from JSON */
			Log.e("Item Stats", "No stats available from json");
		}
		/** Durability */
		if (gear.get(item).maxDurability != 0) {
			tv = new TextView(getActivity());
			int i = gear.get(item).maxDurability;
			tv.setText("Durability " + i + "/" + i);
			tv.setTextColor(getResources().getColor(R.color.common));
			llDurability.addView(tv);
		}
		/** Item Spells */
		if (gear.get(item).itemSpell != null) {
			TextView tvItemSpell = (TextView) popupView.findViewById(R.id.tvpopItemSpell);
			String s = "Use: ";
			if (gear.get(item).itemSpellTrigger != null) {
				if (gear.get(item).itemSpellTrigger.equals("ON_USE")) {
					s = "Use: ";
				} else if (gear.get(item).itemSpellTrigger.equals("ON_EQUIP")) {
					s = "Equip: ";
				} else {
					Log.e("ITEM SPELL FRAGMENT 2", "UNHANDLED ITEM SPELL TRIGGER TYPE");
				}
			}
			tvItemSpell.setText(s + gear.get(item).itemSpell);
			tvItemSpell.setTextColor(getResources().getColor(R.color.uncommon));
			tvItemSpell.setVisibility(View.VISIBLE);
		}
		/** Sockets */
		if (gear.get(item).hasSockets) {
			CheckForGems checkGems = new CheckForGems();
			checkGems.getGems(popupView, getActivity());
		}
		/** Required Level */
		if (gear.get(item).requiredLevel > 1) {
			tv = new TextView(getActivity());
			tv.setText("Requires Level " + gear.get(item).requiredLevel);
			tv.setTextColor(getResources().getColor(R.color.common));
			llDurability.addView(tv);
		}
		/** Sell Price */
		if (gear.get(item).sellPrice > 0) {
			SellPrice.getPrice(popupView, true);
		} else {
			/** No item sell price */
			SellPrice.getPrice(popupView, false);
		}
		setViewsEnabled(true);
	}

	private void setViewsEnabled(boolean b) {
		iv1.setEnabled(b);
		iv2.setEnabled(b);
		iv3.setEnabled(b);
		iv4.setEnabled(b);
		iv5.setEnabled(b);
		iv6.setEnabled(b);
		iv7.setEnabled(b);
		iv8.setEnabled(b);
		iv9.setEnabled(b);
		iv10.setEnabled(b);
		iv11.setEnabled(b);
		iv12.setEnabled(b);
		iv13.setEnabled(b);
		iv14.setEnabled(b);
		iv15.setEnabled(b);
		iv16.setEnabled(b);
		iv17.setEnabled(b);
		iv18.setEnabled(b);
	}
	
	private void initViews(View rootView) {
		iv1 = (ImageView) rootView.findViewById(R.id.ivEquippedHead);
		iv2 = (ImageView) rootView.findViewById(R.id.ivEquippedNeck);
		iv3 = (ImageView) rootView.findViewById(R.id.ivEquippedShoulder);
		iv4 = (ImageView) rootView.findViewById(R.id.ivEquippedBack);
		iv5 = (ImageView) rootView.findViewById(R.id.ivEquippedChest);
		iv6 = (ImageView) rootView.findViewById(R.id.ivEquippedShirt);
		iv7 = (ImageView) rootView.findViewById(R.id.ivEquippedTabard);
		iv8 = (ImageView) rootView.findViewById(R.id.ivEquippedWrist);
		iv9 = (ImageView) rootView.findViewById(R.id.ivEquippedHands);
		iv10 = (ImageView) rootView.findViewById(R.id.ivEquippedWaist);
		iv11 = (ImageView) rootView.findViewById(R.id.ivEquippedLegs);
		iv12 = (ImageView) rootView.findViewById(R.id.ivEquippedFeet);
		iv13 = (ImageView) rootView.findViewById(R.id.ivEquippedFinger1);
		iv14 = (ImageView) rootView.findViewById(R.id.ivEquippedFinger2);
		iv15 = (ImageView) rootView.findViewById(R.id.ivEquippedTrinket1);
		iv16 = (ImageView) rootView.findViewById(R.id.ivEquippedTrinket2);
		iv17 = (ImageView) rootView.findViewById(R.id.ivEquippedMainHand);
		iv18 = (ImageView) rootView.findViewById(R.id.ivEquippedOffHand);
		ivP = (ImageView) rootView.findViewById(R.id.ivPortraitFragment2);
		tvCharClass = (TextView) rootView.findViewById(R.id.tvEquipmentClass);
		tvCharLevel = (TextView) rootView.findViewById(R.id.tvEquipmentLevel);
		tvCharName = (TextView) rootView.findViewById(R.id.tvEquipmentName);
		tvCharRace = (TextView) rootView.findViewById(R.id.tvEquipmentRace);
	}
}