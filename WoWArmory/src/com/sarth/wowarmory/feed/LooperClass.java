package com.sarth.wowarmory.feed;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.widget.ListView;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.LoadingDialogFragment;
import com.sarth.wowarmory.adapters.FeedsListAdapter;

public class LooperClass {

	private JSONObject jsonGet;
	private JSONArray jArr;
	private ArrayList<Object> feedArray;
	private LoadingDialogFragment loadingFragment;
	private Context context;
	private ListView lv;

	public LooperClass(JSONObject jsonGet, ArrayList<Object> feedArray, Context context, ListView lv, LoadingDialogFragment loadingFragment) {
		this.jsonGet = jsonGet;
		this.feedArray = feedArray;
		this.loadingFragment = loadingFragment;
		this.context = context;
		this.lv = lv;
	}

	public void arrayListLoop(ArrayList<Object> feedArray, Context c, ListView lv, JSONObject jsonGet) {
		try {
			jArr = jsonGet.getJSONArray("feed");
			for (int i = Commons.INST.feedPosition; Commons.INST.feedPosition < jArr.length(); i++) {
				JSONObject j = jArr.getJSONObject(i);
				String s = j.getString("type");
				if (s.equals("ACHIEVEMENT")) {
					FeedAchievement achiv = new FeedAchievement();
					achiv.achivDesc = j.getJSONObject("achievement").getString("description");
					achiv.achivIcon = j.getJSONObject("achievement").getString("icon");
					achiv.achivPoints = j.getJSONObject("achievement").getInt("points");
					achiv.achivTimeStamp = j.getString("timestamp");
					achiv.achivTitle = j.getJSONObject("achievement").getString("title");
					achiv.featOfStrength = j.getBoolean("featOfStrength");
					feedArray.add(achiv);
					Commons.INST.feedPosition++;
				} else if (s.equals("BOSSKILL")) {
					FeedBossKill boss = new FeedBossKill();
					boss.killTimeStamp = j.getString("timestamp");
					boss.killIcon = j.getJSONObject("achievement").getString("icon");
					boss.killQuantity = j.getInt("quantity");
					boss.killTitle = j.getJSONObject("achievement").getString("title");
					boss.killPoints = j.getJSONObject("achievement").getInt("points");
					feedArray.add(boss);
					Commons.INST.feedPosition++;
				} else if (s.equals("CRITERIA")) {
					FeedCriteria crit = new FeedCriteria();
					crit.criteriaAchivDesc = j.getJSONObject("achievement").getString("description");
					crit.criteriaAchivTitle = j.getJSONObject("achievement").getString("title");
					crit.criteriaCriteriaDesc = j.getJSONObject("criteria").getString("description");
					crit.criteriaTimeStamp = j.getString("timestamp");
					feedArray.add(crit);
					Commons.INST.feedPosition++;
				} else if (s.equals("LOOT")) {
					GetItemName get = new GetItemName(j.getInt("itemId"), feedArray, jsonGet, c, lv, loadingFragment);
					get.execute();
					break;
				} else {
					Log.e("Feeds", "Unhandled feed type - " + s);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (Commons.INST.feedPosition == jArr.length()) {
			FeedsListAdapter adap = new FeedsListAdapter(c, android.R.layout.simple_spinner_dropdown_item, feedArray);
			lv.setAdapter(adap);
			Commons.INST.fragment8done = true;
			loadingFragment.dismiss();
		}
	}
}