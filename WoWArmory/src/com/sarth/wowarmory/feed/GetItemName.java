package com.sarth.wowarmory.feed;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.LoadingDialogFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

public class GetItemName extends AsyncTask<String, Void, String> {

	private int lootId;
	private ArrayList<Object> feedArray;
	private LoadingDialogFragment loadingFragment;
	private JSONObject jsonGet;
	private JSONObject jsonItem;
	private Context context;
	private ListView lv;

	public GetItemName(int lootId, ArrayList<Object> feedArray, JSONObject jsonGet, Context context, ListView lv,
			LoadingDialogFragment loadingFragment) {
		this.lootId = lootId;
		this.feedArray = feedArray;
		this.loadingFragment = loadingFragment;
		this.jsonGet = jsonGet;
		this.context = context;
		this.lv = lv;
	}

	@Override
	protected String doInBackground(String... params) {
		try {
			HttpClient client = new DefaultHttpClient();
			String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/item/" + lootId;
			StringBuilder url = new StringBuilder(adres);
			HttpGet hg = new HttpGet(url.toString());
			HttpResponse hr = client.execute(hg);
			int status = hr.getStatusLine().getStatusCode();
			if (status == 200) {
				HttpEntity he = hr.getEntity();
				String data = EntityUtils.toString(he);
				jsonItem = new JSONObject(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		try {
			JSONArray jArr = jsonGet.getJSONArray("feed");
			JSONObject j = jArr.getJSONObject(Commons.INST.feedPosition);
			final FeedLoot loot = new FeedLoot();
			loot.itemName = jsonItem.getString("name");
			loot.itemIcon = jsonItem.getString("icon");
			loot.itemQuality = Commons.getItemColor(jsonItem.getInt("quality"));
			loot.lootTimeStamp = j.getString("timestamp");
			feedArray.add(loot);
			Commons.INST.feedPosition++;
			LooperClass loop = new LooperClass(jsonGet, feedArray, context, lv, loadingFragment);
			loop.arrayListLoop(feedArray, context, lv, jsonGet);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}