package com.sarth.wowarmory;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class LoadingDialogFragment extends DialogFragment {

	public static LoadingDialogFragment newInstance(String text) {
		LoadingDialogFragment f = new LoadingDialogFragment();
		Bundle args = new Bundle();
		args.putString("text", text);
		f.setArguments(args);
		return f;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String text = getArguments().getString("text");
		ProgressDialog progressDialog = new ProgressDialog(getActivity());
		progressDialog.setIndeterminate(true);
		if (text != null) {
			progressDialog.setMessage(text);
			progressDialog.setCanceledOnTouchOutside(false);
		}
		return progressDialog;
	}
}