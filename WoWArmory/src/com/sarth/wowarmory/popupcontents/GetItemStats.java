package com.sarth.wowarmory.popupcontents;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;

public class GetItemStats {

	private String reforgedTo = null;
	private String reforgedFrom = null;
	private View popupView;
	private TextView tv;
	private Context context;

	public GetItemStats(View popupView, Context context) {
		super();
		this.popupView = popupView;
		this.context = context;
	}

	public void getStats() {
		LinearLayout llWhites = (LinearLayout) popupView.findViewById(R.id.tvpopWhiteStats);
		LinearLayout llGreens = (LinearLayout) popupView.findViewById(R.id.tvpopGreenStats);
		for (int i = 0; i < Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.size(); i++) {
			switch (Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).stat) {
			case 3:
				tv = new TextView(context);
				tv.setText(String.format(context.getResources().getString(R.string.itemStatId3),
						Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				tv.setTextColor(context.getResources().getColor(R.color.common));
				llWhites.addView(tv);
				break;
			case 4:
				tv = new TextView(context);
				tv.setText(String.format(context.getResources().getString(R.string.itemStatId4),
						Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				tv.setTextColor(context.getResources().getColor(R.color.common));
				llWhites.addView(tv);
				break;
			case 5:
				tv = new TextView(context);
				tv.setText(String.format(context.getResources().getString(R.string.itemStatId5),
						Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				tv.setTextColor(context.getResources().getColor(R.color.common));
				llWhites.addView(tv);
				break;
			case 6:
				tv = new TextView(context);
				if (reforgedTo != null && reforgedTo.equals("Spirit")) {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId6),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount)
							+ " (Reforged from " + reforgedFrom + ")");
				} else {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId6),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				}
				tv.setTextColor(context.getResources().getColor(R.color.common));
				llWhites.addView(tv);

				break;
			case 7:
				tv = new TextView(context);
				tv.setText(String.format(context.getResources().getString(R.string.itemStatId7),
						Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				tv.setTextColor(context.getResources().getColor(R.color.common));
				llWhites.addView(tv);
				break;
			case 13:
				tv = new TextView(context);
				if (reforgedTo != null && reforgedTo.equals("Dodge")) {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId13),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount)
							+ " (Reforged from " + reforgedFrom + ")");
				} else {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId13),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				}
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);

				break;
			case 14:
				tv = new TextView(context);
				if (reforgedTo != null && reforgedTo.equals("Parry")) {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId14),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount)
							+ " (Reforged from " + reforgedFrom + ")");
				} else {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId14),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				}
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);

				break;
			case 31:
				tv = new TextView(context);
				if (reforgedTo != null && reforgedTo.equals("Hit")) {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId31),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount)
							+ " (Reforged from " + reforgedFrom + ")");
				} else {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId31),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				}
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);

				break;
			case 32:
				tv = new TextView(context);
				if (reforgedTo != null && reforgedTo.equals("Critical Strike")) {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId32),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount)
							+ " (Reforged from " + reforgedFrom + ")");
				} else {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId32),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				}
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);

				break;
			case 35:
				tv = new TextView(context);
				tv.setText(String.format(context.getResources().getString(R.string.itemStatId35),
						Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);
				break;
			case 36:
				tv = new TextView(context);
				if (reforgedTo != null && reforgedTo.equals("Haste")) {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId36),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount)
							+ " (Reforged from " + reforgedFrom + ")");
				} else {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId36),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				}
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);

				break;
			case 37:
				tv = new TextView(context);
				if (reforgedTo != null && reforgedTo.equals("Expertise")) {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId37),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount)
							+ " (Reforged from " + reforgedFrom + ")");
				} else {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId37),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				}
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);
				break;
			case 45:
				tv = new TextView(context);
				tv.setText(String.format(context.getResources().getString(R.string.itemStatId45),
						Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);
				break;
			case 49:
				tv = new TextView(context);
				if (reforgedTo != null && reforgedTo.equals("Mastery")) {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId49),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount)
							+ " (Reforged from " + reforgedFrom + ")");
				} else {
					tv.setText(String.format(context.getResources().getString(R.string.itemStatId49),
							Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				}
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);

				break;
			case 57:
				tv = new TextView(context);
				tv.setText(String.format(context.getResources().getString(R.string.itemStatId57),
						Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).itemStat.get(i).amount));
				tv.setTextColor(context.getResources().getColor(R.color.uncommon));
				llGreens.addView(tv);
				break;
			default:
				break;
			}
		}
	}
}