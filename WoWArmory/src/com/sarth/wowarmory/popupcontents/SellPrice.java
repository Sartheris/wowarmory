package com.sarth.wowarmory.popupcontents;

import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SellPrice {

	public static void getPrice(View popupView, boolean b) {
		TextView tvPrice = (TextView) popupView.findViewById(R.id.tvpopCoinSellPrice);
		TextView tvGold = (TextView) popupView.findViewById(R.id.tvpopCoinGold);
		TextView tvSilver = (TextView) popupView.findViewById(R.id.tvpopCoinSilver);
		TextView tvCopper = (TextView) popupView.findViewById(R.id.tvpopCoinCopper);
		ImageView ivGold = (ImageView) popupView.findViewById(R.id.tvpopCoinGoldImageView);
		ImageView ivSilver = (ImageView) popupView.findViewById(R.id.tvpopCoinSilverImageView);
		ImageView ivCopper = (ImageView) popupView.findViewById(R.id.tvpopCoinCopperImageView);

		if (b) {
			int sellPrice = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).sellPrice;
			int gold = 0;
			int silver = 0;
			int copper = 0;
			if (sellPrice > 10000) {
				gold = (int) sellPrice / 10000;
			}
			if (sellPrice > 100) {
				silver = (int) sellPrice / 100;
				String rawsilver = (String.valueOf(silver));
				String formattedsilver = rawsilver.substring(rawsilver.length() - 2);
				silver = Integer.parseInt(formattedsilver);
			}
			String rawcopper = null;
			if (sellPrice < 10) {
				rawcopper = (String.valueOf(sellPrice));
				copper = Integer.parseInt(rawcopper);
			} else {
				rawcopper = (String.valueOf(sellPrice));
				String formattedcopper = rawcopper.substring(rawcopper.length() - 2);
				copper = Integer.parseInt(formattedcopper);
			}
			if (gold > 0) {
				tvPrice.setVisibility(View.VISIBLE);
				tvGold.setText(String.valueOf(gold));
				tvSilver.setText(String.valueOf(silver));
				tvCopper.setText(String.valueOf(copper));
				tvGold.setVisibility(View.VISIBLE);
				tvSilver.setVisibility(View.VISIBLE);
				tvCopper.setVisibility(View.VISIBLE);
				ivGold.setVisibility(View.VISIBLE);
				ivSilver.setVisibility(View.VISIBLE);
				ivCopper.setVisibility(View.VISIBLE);
				if (silver == 0) {
					tvSilver.setVisibility(View.GONE);
					ivSilver.setVisibility(View.GONE);
				}
				if (copper == 0) {
					tvCopper.setVisibility(View.GONE);
					ivCopper.setVisibility(View.GONE);
				}
			} else if (silver > 0 && gold == 0) {
				tvPrice.setVisibility(View.VISIBLE);
				tvSilver.setText(String.valueOf(silver));
				tvCopper.setText(String.valueOf(copper));
				tvGold.setVisibility(View.GONE);
				tvSilver.setVisibility(View.VISIBLE);
				tvCopper.setVisibility(View.VISIBLE);
				ivGold.setVisibility(View.GONE);
				ivSilver.setVisibility(View.VISIBLE);
				ivCopper.setVisibility(View.VISIBLE);
				if (copper == 0) {
					tvCopper.setVisibility(View.GONE);
					ivCopper.setVisibility(View.GONE);
				}

			} else if (silver == 0 && gold == 0) {
				tvPrice.setVisibility(View.VISIBLE);
				tvCopper.setText(String.valueOf(copper));
				tvGold.setVisibility(View.GONE);
				tvSilver.setVisibility(View.GONE);
				tvCopper.setVisibility(View.VISIBLE);
				ivGold.setVisibility(View.GONE);
				ivSilver.setVisibility(View.GONE);
				ivCopper.setVisibility(View.VISIBLE);
			}
		} else {
			tvPrice.setVisibility(View.GONE);
			tvGold.setVisibility(View.GONE);
			tvSilver.setVisibility(View.GONE);
			tvCopper.setVisibility(View.GONE);
			ivGold.setVisibility(View.GONE);
			ivSilver.setVisibility(View.GONE);
			ivCopper.setVisibility(View.GONE);
		}
	}
}