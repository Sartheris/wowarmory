package com.sarth.wowarmory.popupcontents;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.popupcontents.CheckForGems;

public class CheckForGems {

	private boolean socket0Match, socket1Match, socket2Match;
	private String sGem0, sSocket0, sGem1, sSocket1, sGem2, sSocket2;

	public void getGems(View popupView, Context context) {
		ImageView ivGem0 = (ImageView) popupView.findViewById(R.id.tvpopGemIcon0);
		ImageView ivGem1 = (ImageView) popupView.findViewById(R.id.tvpopGemIcon1);
		ImageView ivGem2 = (ImageView) popupView.findViewById(R.id.tvpopGemIcon2);
		TextView tvGem0 = (TextView) popupView.findViewById(R.id.tvpopGem0);
		TextView tvGem1 = (TextView) popupView.findViewById(R.id.tvpopGem1);
		TextView tvGem2 = (TextView) popupView.findViewById(R.id.tvpopGem2);
		int numberOfEquippedGems = 0;
		if (Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo != null
				&& Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets != null) {
			switch (Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.numberOfSockets) {
			case 1:
				ivGem0.setVisibility(View.VISIBLE);
				ivGem1.setVisibility(View.GONE);
				ivGem2.setVisibility(View.GONE);
				tvGem0.setVisibility(View.VISIBLE);
				tvGem1.setVisibility(View.GONE);
				tvGem2.setVisibility(View.GONE);
				break;
			case 2:
				ivGem0.setVisibility(View.VISIBLE);
				ivGem1.setVisibility(View.VISIBLE);
				ivGem2.setVisibility(View.GONE);
				tvGem0.setVisibility(View.VISIBLE);
				tvGem1.setVisibility(View.VISIBLE);
				tvGem2.setVisibility(View.GONE);
				break;
			case 3:
				ivGem0.setVisibility(View.VISIBLE);
				ivGem1.setVisibility(View.VISIBLE);
				ivGem2.setVisibility(View.VISIBLE);
				tvGem0.setVisibility(View.VISIBLE);
				tvGem1.setVisibility(View.VISIBLE);
				tvGem2.setVisibility(View.VISIBLE);
				break;
			default:
				break;
			}
			// Gem 0
			if (Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0 != null) {
				String gemUrl = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/"
						+ Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Icon + ".jpg";
				ImageLoader.getInstance().displayImage(gemUrl, ivGem0);
				tvGem0.setText(Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0);
				numberOfEquippedGems++;
			} else {
				tvGem0.setText(Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket0Type + " Socket");
			}
			// Gem 1
			if (Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem1 != null) {
				String gemUrl = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/"
						+ Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Icon + ".jpg";
				ImageLoader.getInstance().displayImage(gemUrl, ivGem1);
				tvGem1.setText(Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem1);
				numberOfEquippedGems++;
			} else {
				tvGem1.setText(Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket1Type + " Socket");
			}
			// Gem 2
			if (Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem2 != null) {
				String gemUrl = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/"
						+ Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Icon + ".jpg";
				ImageLoader.getInstance().displayImage(gemUrl, ivGem2);
				tvGem2.setText(Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem2);
				numberOfEquippedGems++;
			} else {
				tvGem2.setText(Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket2Type + " Socket");
			}

			/** Socket Bonus */
			/** Check number of sockets and number of gems */

			if (Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.numberOfSockets == numberOfEquippedGems) {
				if (Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.socketBonus != null) {
					String bonus = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.socketBonus;
					TextView tvGemBonus = (TextView) popupView.findViewById(R.id.tvpopSocketBonus);
					tvGemBonus.setText(context.getString(R.string.socket_bonus) + bonus);
					CheckSocketBonus check = new CheckSocketBonus();
					switch (numberOfEquippedGems) {
					case 1:
						sGem0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Type;
						sSocket0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket0Type;
						socket0Match = check.checkForSocketMatch(sGem0, sSocket0);
						if (socket0Match) {
							tvGemBonus.setVisibility(View.VISIBLE);
						} else {
							tvGemBonus.setTextColor(context.getResources().getColor(R.color.poor));
							tvGemBonus.setVisibility(View.VISIBLE);
						}
						break;
					case 2:
						sGem0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Type;
						sSocket0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket0Type;
						sGem1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Type;
						sSocket1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket1Type;
						socket0Match = check.checkForSocketMatch(sGem0, sSocket0);
						socket1Match = check.checkForSocketMatch(sGem1, sSocket1);
						if (socket0Match && socket1Match) {
							tvGemBonus.setVisibility(View.VISIBLE);
						} else {
							tvGemBonus.setTextColor(context.getResources().getColor(R.color.poor));
							tvGemBonus.setVisibility(View.VISIBLE);
						}
						break;
					case 3:
						sGem0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Type;
						sSocket0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket0Type;
						sGem1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Type;
						sSocket1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket1Type;
						sGem2 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Type;
						sSocket2 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket2Type;
						socket0Match = check.checkForSocketMatch(sGem0, sSocket0);
						socket1Match = check.checkForSocketMatch(sGem1, sSocket1);
						socket2Match = check.checkForSocketMatch(sGem2, sSocket2);
						if (socket0Match && socket1Match && socket2Match) {
							tvGemBonus.setVisibility(View.VISIBLE);
						} else {
							tvGemBonus.setTextColor(context.getResources().getColor(R.color.poor));
							tvGemBonus.setVisibility(View.VISIBLE);
						}
						break;
					default:
						break;
					}
				}
				/** Has bonus socket */
			} else if (numberOfEquippedGems > Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.numberOfSockets
					&& Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).extraSocket == true) {

				sGem0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Type;
				sSocket0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket0Type;
				sGem1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Type;
				sSocket1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket1Type;
				sGem2 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Type;
				sSocket2 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket2Type;
				String bonus = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.socketBonus;
				TextView tvGemBonus = (TextView) popupView.findViewById(R.id.tvpopSocketBonus);
				tvGemBonus.setText(context.getString(R.string.socket_bonus) + bonus);
				CheckSocketBonus check = new CheckSocketBonus();
				if (sGem0 != null && sSocket0 == null) {
					// has bonus socket on 0, 0 will not be checked
					sGem1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Type;
					sSocket1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket1Type;
					sGem2 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Type;
					sSocket2 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket2Type;
					socket1Match = check.checkForSocketMatch(sGem1, sSocket1);
					socket2Match = check.checkForSocketMatch(sGem2, sSocket2);
					if (socket1Match && socket2Match) {
						tvGemBonus.setVisibility(View.VISIBLE);
						ivGem0.setVisibility(View.VISIBLE);
						tvGem0.setVisibility(View.VISIBLE);
					} else {
						tvGemBonus.setTextColor(context.getResources().getColor(R.color.poor));
						tvGemBonus.setVisibility(View.VISIBLE);
					}
				}
				if (sGem1 != null && sSocket1 == null) {
					// has bonus socket on 1, 1 will not be checked
					sGem0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Type;
					sSocket0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket0Type;
					sGem2 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem2Type;
					sSocket2 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket2Type;
					socket0Match = check.checkForSocketMatch(sGem0, sSocket0);
					socket2Match = check.checkForSocketMatch(sGem2, sSocket2);
					if (socket0Match && socket2Match) {
						tvGemBonus.setVisibility(View.VISIBLE);
						ivGem1.setVisibility(View.VISIBLE);
						tvGem1.setVisibility(View.VISIBLE);
					} else {
						tvGemBonus.setTextColor(context.getResources().getColor(R.color.poor));
						tvGemBonus.setVisibility(View.VISIBLE);
					}
				}
				if (sGem2 != null && sSocket2 == null) {
					// has bonus socket on 2, 2 will not be checked
					sGem0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem0Type;
					sSocket0 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket0Type;
					sGem1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).tooltipParamsInfo.gem1Type;
					sSocket1 = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem).socketInfo.sockets.socket1Type;
					socket0Match = check.checkForSocketMatch(sGem0, sSocket0);
					socket1Match = check.checkForSocketMatch(sGem1, sSocket1);
					if (socket0Match && socket1Match) {
						tvGemBonus.setVisibility(View.VISIBLE);
						ivGem2.setVisibility(View.VISIBLE);
						tvGem2.setVisibility(View.VISIBLE);
					} else {
						tvGemBonus.setTextColor(context.getResources().getColor(R.color.poor));
						tvGemBonus.setVisibility(View.VISIBLE);
					}
				}
			}
		} else {
			tvGem0.setVisibility(View.GONE);
			ivGem0.setVisibility(View.GONE);
			tvGem1.setVisibility(View.GONE);
			ivGem1.setVisibility(View.GONE);
			tvGem2.setVisibility(View.GONE);
			ivGem2.setVisibility(View.GONE);
		}
	}
}