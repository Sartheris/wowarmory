package com.sarth.wowarmory.popupcontents;

public class CheckSocketBonus {

	public boolean checkForSocketMatch(String sGem, String sSocket) {
		boolean b = false;
		if (sSocket.equals("RED")) {
			if (sGem.equals("RED") || sGem.equals("PURPLE") || sGem.equals("ORANGE") || sGem.equals("PRISMATIC")) {
				b = true;
			}
			if (sGem.equals("BLUE") || sGem.equals("GREEN") || sGem.equals("YELLOW")) {
				b = false;
			}
		}
		if (sSocket.equals("BLUE")) {
			if (sGem.equals("BLUE") || sGem.equals("PURPLE") || sGem.equals("GREEN") || sGem.equals("PRISMATIC")) {
				b = true;
			}
			if (sGem.equals("RED") || sGem.equals("ORANGE") || sGem.equals("YELLOW")) {
				b = false;
			}
		}
		if (sSocket.equals("YELLOW")) {
			if (sGem.equals("YELLOW") || sGem.equals("ORANGE") || sGem.equals("GREEN") || sGem.equals("PRISMATIC")) {
				b = true;
			}
			if (sGem.equals("RED") || sGem.equals("PURPLE") || sGem.equals("BLUE")) {
				b = false;
			}
		}
		if (sSocket.equals("META")) {
			if (sGem.equals("META")) {
				b = true;
			} else {
				b = false;
			}
		}
		if (sSocket.equals("COGWHEEL")) {
			if (sGem.equals("COGWHEEL")) {
				b = true;
			} else {
				b = false;
			}
		}
		return b;
	}
}