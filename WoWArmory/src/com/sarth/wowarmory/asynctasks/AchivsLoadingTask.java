package com.sarth.wowarmory.asynctasks;

import java.util.ArrayList;
import com.sarth.wowarmory.achievements.AchievementCategory;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.sqlite.DataHelper;
import android.content.Context;
import android.os.AsyncTask;

public class AchivsLoadingTask extends AsyncTask<String, Void, String> {

	private ArrayList<AchievementCategory> categories;
	private JSONParserCallback delegate;
	private DataHelper dh;
	private Context context;

	public AchivsLoadingTask(JSONParserCallback callback, Context context, ArrayList<AchievementCategory> cats) {
		this.delegate = callback;
		this.context = context;
		this.categories = cats;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dh = new DataHelper(context);
	}

	@Override
	protected String doInBackground(String... params) {
		dh.insertAchievements(categories);
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		delegate.onFinishedParsing(null);
	}
}