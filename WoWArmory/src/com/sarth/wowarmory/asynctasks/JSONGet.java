package com.sarth.wowarmory.asynctasks;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import android.os.AsyncTask;

public class JSONGet extends AsyncTask<String, Void, String> {

	private String adres;
	private JSONObject jsonGet;
	private JSONParserCallback delegate;

	public JSONGet(JSONParserCallback callback, String adres) {
		this.delegate = callback;
		this.adres = adres;
	}

	@Override
	protected String doInBackground(String... params) {
		try {
			HttpClient client = new DefaultHttpClient();
			StringBuilder url = new StringBuilder(adres);
			HttpGet hg = new HttpGet(url.toString());
			HttpResponse hr = client.execute(hg);
			int status = hr.getStatusLine().getStatusCode();
			if (status == 200) {
				HttpEntity he = hr.getEntity();
				String data = EntityUtils.toString(he);
				jsonGet = new JSONObject(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		delegate.onFinishedParsing(jsonGet);
	}
}