package com.sarth.wowarmory.asynctasks;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.achievements.CurrentCharacterAchievs;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import android.content.Context;
import android.os.AsyncTask;

public class CurrentCharLoadingTask extends AsyncTask<String, Void, String> {

	private ArrayList<CurrentCharacterAchievs> ccaArray;
	private CurrentCharacterAchievs cca;
	private JSONParserCallback delegate;
	private JSONObject jsonGet;
	private Context context;

	public CurrentCharLoadingTask(JSONParserCallback callback, Context context, JSONObject jsonGet) {
		this.delegate = callback;
		this.jsonGet = jsonGet;
		this.context = context;
	}

	@Override
	protected String doInBackground(String... params) {
		try {
			cca = new CurrentCharacterAchievs();
			ccaArray = new ArrayList<CurrentCharacterAchievs>();
			ccaArray = cca.fromJson(jsonGet);
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		LoadingTask load = new LoadingTask(delegate, context, ccaArray, jsonGet);
		load.execute();
	}

}