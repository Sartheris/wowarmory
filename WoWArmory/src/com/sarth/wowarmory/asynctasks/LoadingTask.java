package com.sarth.wowarmory.asynctasks;

import java.util.ArrayList;
import org.json.JSONObject;
import com.sarth.wowarmory.achievements.CurrentCharacterAchievs;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.sqlite.DataHelper;
import android.content.Context;
import android.os.AsyncTask;

public class LoadingTask extends AsyncTask<String, Void, String> {

	private ArrayList<CurrentCharacterAchievs> ccaArray;
	private JSONParserCallback delegate;
	private JSONObject jsonGet;
	private DataHelper dh;
	private Context context;

	public LoadingTask(JSONParserCallback callback, Context context, ArrayList<CurrentCharacterAchievs> ccaArray, JSONObject jsonGet) {
		this.delegate = callback;
		this.context = context;
		this.jsonGet = jsonGet;
		this.ccaArray = ccaArray;
	}

	@Override
	protected String doInBackground(String... params) {
		dh = new DataHelper(context);
		dh.updateCurrentCharacterAchivs(ccaArray);
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		delegate.onFinishedParsing(jsonGet);
	}
}