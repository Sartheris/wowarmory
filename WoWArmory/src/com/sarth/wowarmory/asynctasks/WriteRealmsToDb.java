package com.sarth.wowarmory.asynctasks;

import java.util.ArrayList;
import org.json.JSONObject;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.models.RealmsInfo;
import com.sarth.wowarmory.sqlite.DataModel;
import com.sarth.wowarmory.sqlite.DataSchema;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class WriteRealmsToDb extends AsyncTask<String, Void, String> {

	private JSONParserCallback delegate;
	private JSONObject jsonGet;
	private Context context;
	private ArrayList<RealmsInfo> ri;

	public WriteRealmsToDb(JSONParserCallback callback, JSONObject jsonGet, Context context) {
		this.delegate = callback;
		this.jsonGet = jsonGet;
		this.context = context;
	}

	@Override
	protected String doInBackground(String... params) {
		ri = new ArrayList<RealmsInfo>();
		ri.addAll(RealmsInfo.fromJson(jsonGet));
		for (int j = 0; j < ri.size(); j++) {
			ContentValues values = new ContentValues();
			values.put(DataSchema.RealmsColumns.REALM_REGION, ri.get(j).realmRegion);
			values.put(DataSchema.RealmsColumns.REALM_NAME, ri.get(j).realm);
			values.put(DataSchema.RealmsColumns.REALM_SLUG, ri.get(j).realmSlug);
			DataModel model = new DataModel(context);
			SQLiteDatabase db = model.getReadableDatabase();
			Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.REALMS_TABLE_NAME + " WHERE " + DataSchema.RealmsColumns.REALM_SLUG
					+ " = ?" + " AND " + DataSchema.RealmsColumns.REALM_REGION + " = ?", new String[] { ri.get(j).realmSlug,
					ri.get(j).realmRegion });
			if (c.getCount() == 0) {
				db.insert(DataSchema.REALMS_TABLE_NAME, null, values);
			} else {
				db.update(DataSchema.REALMS_TABLE_NAME, values, DataSchema.RealmsColumns.REALM_SLUG + " = ?",
						new String[] { ri.get(j).realmSlug });
			}
			c.close();
			db.close();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		delegate.onFinishedParsing(null);
	}
}