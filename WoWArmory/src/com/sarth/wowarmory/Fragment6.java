package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.adapters.TitlesListAdapter;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Fragment6 extends Fragment implements JSONParserCallback {

	private JSONGet taskGet;
	private ListView lv;
	private String imgUrl;

	public Fragment6() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment6, container, false);
		lv = (ListView) rootView.findViewById(R.id.lvTitles);
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (menuVisible) {
			if (!Commons.INST.fragment6done) {
				String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
						+ "?fields=titles" + "&locale=" + Commons.INST.locale;
				taskGet = new JSONGet(this, adres);
				taskGet.execute();
			}
		} else {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		String thumbnailId = Commons.INST.charThumbnail.replace(Commons.INST.realmSlug, "").replace("-avatar.jpg", "");
		imgUrl = "http://" + Commons.INST.regionUrl + ".battle.net/static-render/" + Commons.INST.regionUrl + "/" + Commons.INST.realmSlug + thumbnailId
				+ "-card.jpg";
		ArrayList<String> titles = new ArrayList<String>();
		titles.add(imgUrl);
		try {
			JSONArray jArr = jsonGet.getJSONArray("titles");
			for (int i = 0; i < jArr.length(); i++) {
				String s = jArr.getJSONObject(i).getString("name");
				titles.add(s);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		TitlesListAdapter adap = new TitlesListAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, titles);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (arg2 == 0) {
					Intent i = new Intent(getActivity(), ShowImage.class);
					i.putExtra("adres", imgUrl);
					startActivity(i);
				}

			}
		});
		lv.setAdapter(adap);
		Commons.INST.fragment6done = true;
	}
}