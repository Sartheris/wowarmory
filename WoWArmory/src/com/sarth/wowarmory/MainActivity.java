package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.sarth.wowarmory.adapters.RealmsStatusListAdapter;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.asynctasks.WriteRealmsToDb;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.models.CharInfo;
import com.sarth.wowarmory.models.ClassInfo;
import com.sarth.wowarmory.models.RaceInfo;
import com.sarth.wowarmory.models.RealmsInfo;
import com.sarth.wowarmory.sqlite.DataHelper;

public class MainActivity extends FragmentActivity implements JSONParserCallback {

	private LoadingDialogFragment loadingFragment;
	private Spinner spinRegion, spinRealm;
	private Button butFindChar, butRealms;
	private SharedPreferences sharedPref;
	private ArrayList<RealmsInfo> ri;
	private int operationId;
	private DataHelper dh;
	private String adres;
	private JSONGet get;
	private EditText et;

	private void setViewsActivation(boolean b) {
		spinRegion.setEnabled(b);
		spinRealm.setEnabled(b);
		et.setEnabled(b);
		butFindChar.setEnabled(b);
		butRealms.setEnabled(b);
	}

	private boolean isOnline(Context ctx) {
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		} else {
			setViewsActivation(false);
			return false;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		et = (EditText) findViewById(R.id.etEnterName);
		spinRegion = (Spinner) findViewById(R.id.spinRegion);
		spinRealm = (Spinner) findViewById(R.id.spinRealm);
		butFindChar = (Button) findViewById(R.id.bFindChar);
		butRealms = (Button) findViewById(R.id.bRealmStatus);
		dh = new DataHelper(getApplicationContext());
		sharedPref = getSharedPreferences("realm", MODE_PRIVATE);
		String s = sharedPref.getString("name", null);
		if (s != null) {
			et.setText(s);
		}
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).discCache(
				new UnlimitedDiscCache(getCacheDir())).build();
		ImageLoader.getInstance().init(config);
	}

	@Override
	protected void onStart() {
		super.onStart();
		/** Check Network Status */
		if (!isOnline(getApplicationContext())) {
			setViewsActivation(false);
			AlertDialog.Builder db = new AlertDialog.Builder(this);
			db.setMessage("This application needs internet connection to work");
			db.setNeutralButton("Open Settings", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent i = new Intent(Settings.ACTION_SETTINGS);
					startActivity(i);
				}
			});
			db.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					finish();
				}
			});
			AlertDialog dlg = db.create();
			dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dlg.setCanceledOnTouchOutside(false);
			dlg.setCancelable(false);
			dlg.show();
		} else {
			setViewsActivation(true);
			addSpinners();
		}
	}

	private void addSpinners() {
		String[] regions = getResources().getStringArray(R.array.regions_array);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, regions);
		spinRegion.setAdapter(adapter);
		int i = sharedPref.getInt("region", -1);
		if (i != -1 && i <= 3) {
			spinRegion.setSelection(i);
		}
		spinRegion.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				getRealmsForRegion(arg2);
				SharedPreferences.Editor prefEditor = sharedPref.edit();
				prefEditor.putInt("region", arg2);
				prefEditor.commit();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	private void getRealmsForRegion(int position) {
		switch (position) {
		case 0:
			Commons.INST.regionUrl = "eu";
			break;
		case 1:
			Commons.INST.regionUrl = "us";
			break;
		case 2:
			Commons.INST.regionUrl = "kr";
			break;
		case 3:
			Commons.INST.regionUrl = "tw";
			break;
		default:
			break;
		}
		if (!Commons.INST.regionUrl.equals("")) {
			/** Check if DB for Realms is available */
			if (dh.selectAllRealms(Commons.INST.regionUrl).size() > 0) {
				ri = dh.selectAllRealms(Commons.INST.regionUrl);
				spinRegion.setEnabled(true);
				spinRealm.setEnabled(true);
				butFindChar.setEnabled(true);
				if (ri != null) {
					RealmsStatusListAdapter adap = new RealmsStatusListAdapter(this, android.R.layout.simple_spinner_dropdown_item, ri, 0);
					spinRealm.setAdapter(adap);
					int i = sharedPref.getInt("realm", -1);
					if (i != -1 && i <= ri.size()) {
						spinRealm.setSelection(i);
					} else if (i > ri.size()) {
						spinRealm.setSelection(0);
					}
					spinRealm.setOnItemSelectedListener(new OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
							Commons.INST.realmSlug = ri.get(arg2).realmSlug;
							SharedPreferences.Editor prefEditor = sharedPref.edit();
							prefEditor.putInt("realm", arg2);
							prefEditor.commit();
						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
						}
					});
				}
			} else {
				spinRegion.setEnabled(false);
				spinRealm.setEnabled(false);
				butFindChar.setEnabled(false);
				String[] loading = getResources().getStringArray(R.array.loading_array);
				ArrayAdapter<String> loadingAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, loading);
				spinRealm.setAdapter(loadingAdapter);
				operationId = 4;
				adres = "https://" + Commons.INST.regionUrl + ".battle.net/api/wow/realm/status" + "?locale=" + Commons.INST.locale;
				startJson(adres);
			}
		}
	}

	/** Button Find Character */
	public void findCharButton(View v) {
		Commons.INST.setFragmentsToLoad();
		Commons.INST.charAchivsNeedUpdating = true;
		loadingFragment = LoadingDialogFragment.newInstance(getString(R.string.loading_char));
		loadingFragment.show(getSupportFragmentManager(), "tag");
		Commons.INST.charName = et.getText().toString();
		SharedPreferences.Editor prefEditor = sharedPref.edit();
		prefEditor.putString("name", Commons.INST.charName);
		prefEditor.commit();
		if (dh.hasRaces() && dh.hasClasses()) {
			operationId = 8;
			onFinishedParsing(null);
		} else {
			operationId = 1;
			adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/data/character/races" + "?locale=" + Commons.INST.locale;
			startJson(adres);
		}
	}

	/** Button Realms Status */
	public void checkRealmsStatus(View v) {
		operationId = 5;
		loadingFragment = LoadingDialogFragment.newInstance("Loading realms status, please wait...");
		loadingFragment.show(getSupportFragmentManager(), "tag");
		adres = "https://" + Commons.INST.regionUrl + ".battle.net/api/wow/realm/status" + "?locale=" + Commons.INST.locale;
		startJson(adres);
	}

	/** Delegate */

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		switch (operationId) {
		case 1:
			operationId = 2;
			dh.insertRaces(RaceInfo.fromJson(jsonGet));
			adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/data/character/classes" + "?locale=" + Commons.INST.locale;
			startJson(adres);
			break;
		case 2:
			operationId = 10;
			dh.insertClasses(ClassInfo.fromJson(jsonGet));
			adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/"
					+ Commons.INST.charName + "?fields=stats" + "&locale=" + Commons.INST.locale;
			startJson(adres);
			break;
		case 4:
			/** Write the database */
			operationId = 6;
			WriteRealmsToDb write = new WriteRealmsToDb(this, jsonGet, getApplicationContext());
			write.execute();
			break;
		case 5:
			try {
				ri = new ArrayList<RealmsInfo>();
				ri.addAll(RealmsInfo.fromJson(jsonGet));
				Intent i = new Intent(MainActivity.this, RealmsStatusActivity.class);
				i.putExtra("realmi", ri);
				loadingFragment.dismiss();
				startActivity(i);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 6:
			ri = dh.selectAllRealms(Commons.INST.regionUrl);
			spinRegion.setEnabled(true);
			butFindChar.setEnabled(true);
			if (ri != null && ri.size() > 0) {
				RealmsStatusListAdapter adap = new RealmsStatusListAdapter(this, android.R.layout.simple_spinner_dropdown_item, ri, 0);
				spinRealm.setAdapter(adap);
				spinRealm.setEnabled(true);
				spinRealm.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
						spinRealm.getSelectedItemPosition();
						Commons.INST.realmSlug = ri.get(arg2).realmSlug;
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
					}
				});
			} else if (ri.size() == 0) {
				String[] cantShow = getResources().getStringArray(R.array.cant_be_shown_array);
				ArrayAdapter<String> cantBeShown = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cantShow);
				spinRealm.setEnabled(false);
				spinRealm.setAdapter(cantBeShown);
			}
			break;
		case 8:
			operationId = 10;
			adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/"
					+ Commons.INST.charName + "?fields=stats" + "&locale=" + Commons.INST.locale;
			startJson(adres);
			break;
		case 10:
			/** Final Call */
			loadingFragment.dismiss();
			if (jsonGet != null) {
				ArrayList<CharInfo> charStuff = new ArrayList<CharInfo>();
				charStuff.add(CharInfo.fromJson(jsonGet));
				dh.getClass(Commons.INST.charClassId);
				dh.getRace(Commons.INST.charRaceId);
				Intent in = new Intent(this, CharActivity.class);
				in.putExtra("json", jsonGet.toString());
				startActivity(in);
				butFindChar.setEnabled(true);
			} else {
				AlertDialog.Builder db = new AlertDialog.Builder(this);
				db.setTitle(R.string.char_not_found_title);
				LayoutInflater factory = LayoutInflater.from(MainActivity.this);
				View v = factory.inflate(R.layout.charnotfound, null);
				db.setView(v);
				db.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				AlertDialog dlg = db.create();
				dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dlg.setCanceledOnTouchOutside(false);
				dlg.setCancelable(false);
				dlg.show();
				butFindChar.setEnabled(true);
			}
			break;
		}
	}

	private void startJson(String adres) {
		get = new JSONGet(this, adres);
		get.execute();
	}
}