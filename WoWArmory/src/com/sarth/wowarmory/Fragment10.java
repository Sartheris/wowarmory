package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.adapters.ProgressionListAdapter;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.progression.Progression;
import com.sarth.wowarmory.progression.ProgressionBoss;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class Fragment10 extends Fragment implements JSONParserCallback {

	private LoadingDialogFragment loadingFragment;
	private JSONGet taskGet;
	private ListView lv;

	public Fragment10() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment10, container, false);
		lv = (ListView) rootView.findViewById(R.id.lvProgression);
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (!menuVisible) {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
			if (loadingFragment != null) {
				loadingFragment.dismiss();
			}
		} else {
			if (!Commons.INST.fragment10done) {
				loadingFragment = LoadingDialogFragment.newInstance("Getting character raid progression...");
				loadingFragment.show(getActivity().getSupportFragmentManager(), "tag");
				String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
						+ "?fields=progression" + "&locale=" + Commons.INST.locale;
				taskGet = new JSONGet(this, adres);
				taskGet.execute();
			}
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		try {
			JSONArray jArr = jsonGet.getJSONObject("progression").getJSONArray("raids");
			ArrayList<Object> arr = new ArrayList<Object>();
			for (int i = 0; i < jArr.length(); i++) {
				Progression prog = new Progression();
				JSONObject j = jArr.getJSONObject(i);
				prog.raidName = j.getString("name");
				prog.normal = j.getInt("normal");
				prog.heroic = j.getInt("heroic");
				JSONArray jArrBosses = j.getJSONArray("bosses");
				prog.boss = new ArrayList<ProgressionBoss>();
				for (int k = 0; k < jArrBosses.length(); k++) {
					ProgressionBoss boss = new ProgressionBoss();
					JSONObject jj = jArrBosses.getJSONObject(k);
					boss.bossName = jj.getString("name");
					if (jj.has("normalKills") && jj.has("normalTimestamp")) {
						boss.normalKills = jj.getInt("normalKills");
						boss.normalTimestamp = jj.getString("normalTimestamp");
					}
					if (jj.has("lfrKills") && jj.has("lfrTimestamp")) {
						boss.lfrKills = jj.getInt("lfrKills");
						boss.lfrTimestamp = jj.getString("lfrTimestamp");
					}
					if (jj.has("heroicKills") && jj.has("heroicTimestamp")) {
						boss.heroicKills = jj.getInt("heroicKills");
						boss.heroicTimestamp = jj.getString("heroicTimestamp");
					}
					if (jj.has("flexKills") && jj.has("flexTimestamp")) {
						boss.flexKills = jj.getInt("flexKills");
						boss.flexTimestamp = jj.getString("flexTimestamp");
					}
					prog.boss.add(boss);
				}
				arr.add(prog);
			}
			ProgressionListAdapter adap = new ProgressionListAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, arr);
			lv.setAdapter(adap);
			loadingFragment.dismiss();
			Commons.INST.fragment10done = true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}