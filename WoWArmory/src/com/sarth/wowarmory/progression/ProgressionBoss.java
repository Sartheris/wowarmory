package com.sarth.wowarmory.progression;

public class ProgressionBoss {

	public String bossName;
	public int lfrKills, normalKills, heroicKills, flexKills;
	public String lfrTimestamp, normalTimestamp, heroicTimestamp, flexTimestamp;
}