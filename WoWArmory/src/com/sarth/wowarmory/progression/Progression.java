package com.sarth.wowarmory.progression;

import java.util.ArrayList;

public class Progression {
	
	public String raidName;
	public int normal, heroic;
	public ArrayList<ProgressionBoss> boss;
}