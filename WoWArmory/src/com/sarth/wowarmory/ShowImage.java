package com.sarth.wowarmory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import com.nostra13.universalimageloader.core.ImageLoader;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

public class ShowImage extends Activity {

	private ImageView iv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_image);
		iv = (ImageView) findViewById(R.id.ivShowCharImage);
		Toast.makeText(this, "Long click on image to save to sd card", Toast.LENGTH_SHORT).show();
		String imgUrl = getIntent().getExtras().getString("adres");
		ImageLoader.getInstance().displayImage(imgUrl, iv);
		iv.setOnLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				vib.vibrate(50);
				AlertDialog.Builder db = new AlertDialog.Builder(ShowImage.this);
				db.setNeutralButton("Save Image", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						BitmapDrawable drawable = (BitmapDrawable) iv.getDrawable();
						Bitmap bitmap = drawable.getBitmap();
						File sdCardDirectory = Environment.getExternalStorageDirectory();
						File image = new File(sdCardDirectory, "wowchar" + System.currentTimeMillis() + ".png");
						boolean success = false;
						try {
							FileOutputStream outStream = new FileOutputStream(image);
							bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
							outStream.flush();
							outStream.close();
							success = true;
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						if (success) {
							Toast.makeText(ShowImage.this, "Image saved to SD Card", Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(ShowImage.this, "Error during image saving", Toast.LENGTH_LONG).show();
						}
					}
				});
				db.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				AlertDialog dlg = db.create();
				dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dlg.show();
				return false;
			}
		});
	}
}