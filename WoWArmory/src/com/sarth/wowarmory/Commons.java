package com.sarth.wowarmory;

import com.sarth.wowarmory.models.CharInfo;

public enum Commons {
	INST;
	public String locale, regionUrl, realmName, realmSlug, charThumbnail;
	public String charName, charClass, charRace, currentItem, charPowerType, charSide;
	public int charLevel, charClassId, charRaceId, averageItemLevel, averageItemLevelEquipped;
	public boolean fragment1done;
	public boolean fragment2done;
	public boolean fragment3done;
	public boolean fragment4done;
	public boolean fragment5done;
	public boolean fragment6done;
	public boolean fragment7done;
	public boolean fragment8done;
	public boolean fragment9done;
	public boolean fragment10done;
	public boolean fragment11done;
	public boolean fragment12done;
	public boolean fragment13done;
	public boolean charAchivsNeedUpdating = true;
	public int feedPosition = 0;
	public static CharInfo currentCharacter;

	private Commons() {
		// Constructor
	}

	public void setFragmentsToLoad() {
		fragment1done = false;
		fragment2done = false;
		fragment3done = false;
		fragment4done = false;
		fragment5done = false;
		fragment6done = false;
		fragment7done = false;
		fragment8done = false;
		fragment9done = false;
		fragment10done = false;
		fragment11done = false;
		fragment12done = false;
		fragment13done = false;
	}

	public static String getItemSubclass(int itemSubClass) {
		switch (itemSubClass) {
		case 0:
			return "Miscellaneous";
		case 1:
			return "Cloth";
		case 2:
			return "Leather";
		case 3:
			return "Mail";
		case 4:
			return "Plate";
		case 6:
			return "Shield";
		default:
			return "";
		}
	}

	public static String getInventoryType(int inventoryType) {
		switch (inventoryType) {
		case 1:
			return "Head";
		case 2:
			return "Neck";
		case 3:
			return "Shoulder";
		case 4:
			return "Shirt";
		case 5:
			return "Chest";
		case 6:
			return "Waist";
		case 7:
			return "Legs";
		case 8:
			return "Feet";
		case 9:
			return "Wrists";
		case 10:
			return "Hands";
		case 11:
			return "Finger";
		case 12:
			return "Trinket";
		case 13:
			return "One-Hand";
		case 14:
			return "Off Hand";
		case 20:
			return "Chest";
		case 26:
			return "Ranged";
		case 15:
			return "Ranged";
		case 17:
			return "Two-Hand";
		case 16:
			return "Back";
		case 19:
			return "Tabard";
		case 21:
			return "Main Hand";
		case 23:
			return "Held In Off-hand";
		default:
			return "";
		}
	}

	public static String getItemType(int itemSubClass) {
		switch (itemSubClass) {
		case 0:
			return "Axe";
		case 1:
			return "Axe";
		case 2:
			return "Bow";
		case 3:
			return "Gun";
		case 4:
			return "Mace";
		case 5:
			return "Mace";
		case 6:
			return "Polearm";
		case 7:
			return "Sword";
		case 8:
			return "Sword";
		case 10:
			return "Staff";
		case 13:
			return "Fist Weapon";
		case 15:
			return "Dagger";
		case 18:
			return "Crossbow";
		case 19:
			return "Wand";
		case 20:
			return "Fishing Pole";
		default:
			return "";
		}
	}

	public static int getItemColor(int quality) {
		switch (quality) {
		case 0:
			return R.color.poor;
		case 1:
			return R.color.common;
		case 2:
			return R.color.uncommon;
		case 3:
			return R.color.rare;
		case 4:
			return R.color.epic;
		case 5:
			return R.color.legendary;
		case 6:
			return R.color.artifact;
		case 7:
			return R.color.artifact;
		default:
			return 0;
		}
	}
}