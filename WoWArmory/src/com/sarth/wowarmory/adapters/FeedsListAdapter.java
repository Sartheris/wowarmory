package com.sarth.wowarmory.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.feed.FeedAchievement;
import com.sarth.wowarmory.feed.FeedBossKill;
import com.sarth.wowarmory.feed.FeedCriteria;
import com.sarth.wowarmory.feed.FeedLoot;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FeedsListAdapter extends ArrayAdapter<Object> {

	private LayoutInflater inflater;
	private ArrayList<Object> feedArray;
	private Context context;

	public FeedsListAdapter(Context context, int resource, ArrayList<Object> feedArray) {
		super(context, resource, feedArray);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.feedArray = feedArray;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		convertView = inflater.inflate(R.layout.item_row_feed, null);
		holder.iv = (ImageView) convertView.findViewById(R.id.ivFeedIcon);
		holder.tvText = (TextView) convertView.findViewById(R.id.tvFeed);
		holder.tvDate = (TextView) convertView.findViewById(R.id.tvFeedDate);
		if (feedArray.get(position) instanceof FeedAchievement) {
			getFeedAchievement(holder, convertView, position);
		} else if (feedArray.get(position) instanceof FeedLoot) {
			getFeedLoot(holder, convertView, position);
		} else if (feedArray.get(position) instanceof FeedBossKill) {
			getFeedBossKill(holder, convertView, position);
		} else if (feedArray.get(position) instanceof FeedCriteria) {
			getFeedCriteria(holder, convertView, position);
		}
		convertView.setTag(holder);
		return convertView;
	}

	private void getFeedCriteria(ViewHolder holder, View convertView, int position) {
		FeedCriteria crit = (FeedCriteria) feedArray.get(position);
		holder.iv.setImageDrawable(context.getResources().getDrawable(R.drawable.feed_criteria));
		String text = "<font color=#FFFFFFFF>Completed step </font> <font color=%s> %s </font> <font color=#FFFFFFFF> of achievement </font> <font color=%s> %s </font>";
		int i = context.getResources().getColor(R.color.itemlevel);
		int step = context.getResources().getColor(R.color.step);
		String f = String.format(text, step, crit.criteriaCriteriaDesc, i, crit.criteriaAchivTitle);
		holder.tvText.setText(Html.fromHtml(f));
		holder.tvDate.setText(getFeedDate(crit.criteriaTimeStamp));
	}

	private void getFeedBossKill(ViewHolder holder, View convertView, int position) {
		FeedBossKill boss = (FeedBossKill) feedArray.get(position);
		holder.iv.setImageDrawable(context.getResources().getDrawable(R.drawable.feed_boss_kill));
		String text = "<font color=#FFFFFFFF>%s </font> <font color=%s> %s </font>";
		String f = String.format(text, boss.killQuantity, context.getResources().getColor(R.color.itemlevel), boss.killTitle);
		holder.tvText.setText(Html.fromHtml(f));
		holder.tvDate.setText(getFeedDate(boss.killTimeStamp));
	}

	private void getFeedAchievement(ViewHolder holder, View convertView, int position) {
		FeedAchievement achiv = (FeedAchievement) feedArray.get(position);
		String imgUrl = String.format(context.getString(R.string.url_item_icon), Commons.INST.regionUrl, achiv.achivIcon);
		ImageLoader.getInstance().displayImage(imgUrl, holder.iv);
		String f;
		if (achiv.featOfStrength) {
			String text = "<font color=#FFFFFFFF>Earned the Feat of Strength </font> <font color=%s> %s</font>";
			f = String.format(text, context.getResources().getColor(R.color.itemlevel), achiv.achivTitle);
		} else {
			String text = "<font color=#FFFFFFFF>Earned the Achievement </font> <font color=%s> %s </font> </font> <font color=#FFFFFFFF>for %s points</font>";
			f = String.format(text, context.getResources().getColor(R.color.achiv), achiv.achivTitle, achiv.achivPoints);
		}
		holder.tvText.setText(Html.fromHtml(f));
		holder.tvDate.setText(getFeedDate(achiv.achivTimeStamp));
	}

	private void getFeedLoot(ViewHolder holder, View convertView, int position) {
		FeedLoot loot = (FeedLoot) feedArray.get(position);
		String imgUrl = String.format(context.getString(R.string.url_item_icon), Commons.INST.regionUrl, loot.itemIcon);
		ImageLoader.getInstance().displayImage(imgUrl, holder.iv);
		String text = "<font color=#FFFFFFFF>Obtained </font> <font color=%s> %s</font>";
		String f = String.format(text, context.getResources().getColor(loot.itemQuality), loot.itemName);
		holder.tvText.setText(Html.fromHtml(f));
		holder.tvDate.setText(getFeedDate(loot.lootTimeStamp));
	}

	private String getFeedDate(String lootTimeStamp) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(lootTimeStamp));
		return formatter.format(calendar.getTime());
	}

	private static class ViewHolder {
		private TextView tvText, tvDate;
		private ImageView iv;
	}
}