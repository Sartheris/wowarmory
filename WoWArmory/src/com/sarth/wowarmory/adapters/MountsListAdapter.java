package com.sarth.wowarmory.adapters;

import java.util.ArrayList;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.mountsandpets.Mounts;
import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MountsListAdapter extends ArrayAdapter<Object> {

	private LayoutInflater inflater;
	private ArrayList<Object> mountsArray;
	private Context context;

	public MountsListAdapter(Context context, int resource, ArrayList<Object> mountsArray) {
		super(context, resource, mountsArray);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mountsArray = mountsArray;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Mounts mm = (Mounts) mountsArray.get(position);
		convertView = inflater.inflate(R.layout.item_row_mount, null);
		holder.tvName = (TextView) convertView.findViewById(R.id.tvMount);
		holder.tvName.setText(mm.mountName);
		holder.tvName.setTextColor(getColor(mm.mountQuality));
		holder.iv = (ImageView) convertView.findViewById(R.id.ivMount);
		String imgUrl = String.format(context.getString(R.string.url_item_icon), Commons.INST.regionUrl, mm.mountIcon);
		ImageLoader.getInstance().displayImage(imgUrl, holder.iv);
		convertView.setTag(holder);
		return convertView;
	}

	private ColorStateList getColor(int mountQuality) {
		switch (mountQuality) {
		case 3:
			return context.getResources().getColorStateList(R.color.rare);
		case 4:
			return context.getResources().getColorStateList(R.color.epic);
		default:
			return context.getResources().getColorStateList(R.color.common);
		}
	}

	private static class ViewHolder {
		private TextView tvName;
		private ImageView iv;
	}
}