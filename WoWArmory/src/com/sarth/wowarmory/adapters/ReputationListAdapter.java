package com.sarth.wowarmory.adapters;

import java.util.ArrayList;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.reputation.Reputation;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ReputationListAdapter extends ArrayAdapter<Object> {

	private LayoutInflater inflater;
	private ArrayList<Object> repArray;
	private Context context;

	public ReputationListAdapter(Context context, int resource, ArrayList<Object> repArray) {
		super(context, resource, repArray);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.repArray = repArray;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Reputation rep = (Reputation) repArray.get(position);
		convertView = inflater.inflate(R.layout.item_row_reputation, null);
		holder.tvName = (TextView) convertView.findViewById(R.id.tvRepName);
		holder.tvName.setText(rep.name);
		holder.tvStanding = (TextView) convertView.findViewById(R.id.tvRepStanding);
		holder.ll = (LinearLayout) convertView.findViewById(R.id.llRepProgress);
		holder.tvStanding.setText(getStanding(rep.standing, rep.max, holder.ll));
		holder.tvProgress = (TextView) convertView.findViewById(R.id.tvRepLevel);
		holder.tvProgress.setText(rep.value + " / " + rep.max);
		holder.ll.getLayoutParams().width = getMeasurements(rep.value, rep.max);
		convertView.setTag(holder);
		return convertView;
	}

	private int getMeasurements(int value, int max) {
		double a = value;
		double b = max;
		double procent = (a / b) * 100;
		WindowManager w = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display d = w.getDefaultDisplay();
		Point size = new Point();
		d.getSize(size);
		double width = size.x;
		double currentOnePercent = width / 100;
		int rezultat = (int) (procent * currentOnePercent);
		return rezultat;
	}

	private String getStanding(int standing, int max, LinearLayout ll) {
		switch (standing) {
		case 0:
			if (max == 8400) {
				return "Stranger";
			} else {
				ll.setBackgroundColor(context.getResources().getColor(R.color.rep_hated));
				return "Hated";
			}
		case 1:
			if (max == 8400) {
				return "Acquaintance";
			} else {
				ll.setBackgroundColor(context.getResources().getColor(R.color.rep_hostile));
				return "Hostile";
			}
		case 2:
			if (max == 8400) {
				return "Buddy";
			} else {
				ll.setBackgroundColor(context.getResources().getColor(R.color.rep_unfriendly));
				return "Unfriendly";
			}
		case 3:
			if (max == 8400) {
				return "Friend";
			} else {
				ll.setBackgroundColor(context.getResources().getColor(R.color.rep_neutral));
				return "Neutral";
			}
		case 4:
			if (max == 8400) {
				return "Good Friend";
			} else {
				ll.setBackgroundColor(context.getResources().getColor(R.color.rep_friendly));
				return "Friendly";
			}
		case 5:
			if (max == 999) {
				return "Best Buddy";
			} else {
				ll.setBackgroundColor(context.getResources().getColor(R.color.rep_honored));
				return "Honored";
			}
		case 6:
			ll.setBackgroundColor(context.getResources().getColor(R.color.rep_revered));
			return "Revered";
		case 7:
			ll.setBackgroundColor(context.getResources().getColor(R.color.rep_exalted));
			return "Exalted";
		default:
			return null;
		}

	}

	private static class ViewHolder {
		private TextView tvName, tvStanding, tvProgress;
		private LinearLayout ll;
	}
}