package com.sarth.wowarmory.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.achievements.CombinedAchivs;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CombinedAchivsListAdapter extends ArrayAdapter<CombinedAchivs> {

	private ArrayList<CombinedAchivs> mItems;
	private LayoutInflater inflater;

	public CombinedAchivsListAdapter(Context context, int resource, ArrayList<CombinedAchivs> aa) {
		super(context, resource, aa);
		this.mItems = aa;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		CombinedAchivs cAchivs = mItems.get(position);
		// Categories available
		if (cAchivs.typeId == 1) {
			if (convertView == null) {
				holder = new ViewHolder();
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			convertView = inflater.inflate(R.layout.achiv_category_element, null);
			holder.catTitle = (TextView) convertView.findViewById(R.id.tvCatRowTitle);
			holder.catTitle.setText(cAchivs.achivCategory.CategoryName);
			holder.typeID = 1;
			convertView.setId(1);
			convertView.setTag(holder);
		} else {
			// No categories
			if (convertView == null) {
				holder = new ViewHolder();
				if (cAchivs.achiv.hasAchiv.equals("1")) {
					convertView = inflater.inflate(R.layout.achiv_element, null);
					holder.achivDate = (TextView) convertView.findViewById(R.id.tvAchivRowDate);
					setRowElements(holder, convertView, cAchivs);
					holder.achivDate.setText(getDate(cAchivs));
				} else {
					if (holder.typeID != 2) {
						holder = new ViewHolder();
						convertView = inflater.inflate(R.layout.achiv_missing_element, null);
						setRowElements(holder, convertView, cAchivs);
					}
				}
				convertView.setId(2);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
				if (holder.typeID != 2) {
					if (cAchivs.achiv.hasAchiv.equals("1")) {
						convertView = inflater.inflate(R.layout.achiv_element, null);
						holder.achivDate = (TextView) convertView.findViewById(R.id.tvAchivRowDate);
						setRowElements(holder, convertView, cAchivs);
						holder.achivDate.setText(getDate(cAchivs));
					} else {
						holder = new ViewHolder();
						convertView = inflater.inflate(R.layout.achiv_missing_element, null);
						setRowElements(holder, convertView, cAchivs);
					}
					convertView.setId(2);
					convertView.setTag(holder);
				}
			}
		}
		return convertView;
	}

	/** Set elements of non category item */
	private void setRowElements(ViewHolder holder, View convertView, CombinedAchivs ent) {
		holder.achivTitle = (TextView) convertView.findViewById(R.id.tvAchivRowTitle);
		holder.achivTitle.setText(ent.achiv.achivTitle);
		holder.achivDesc = (TextView) convertView.findViewById(R.id.tvAchivRowDesc);
		holder.achivDesc.setText(ent.achiv.achivDescription);
		holder.achivPoints = (TextView) convertView.findViewById(R.id.ivAchivRowPoints);
		holder.achivPoints.setText("" + ent.achiv.achivPoints);
		holder.achivIcon = (ImageView) convertView.findViewById(R.id.ivAchivRowIcon);
		String imgUrl = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/" + ent.achiv.achivIcon + ".jpg";
		ImageLoader.getInstance().displayImage(imgUrl, holder.achivIcon);
	}

	/** Get Date */
	private String getDate(CombinedAchivs ent) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(ent.achiv.achivDate));
		String s = formatter.format(calendar.getTime());
		return s;
	}

	private static class ViewHolder {
		private TextView achivDate;
		private int typeID;
		private TextView achivTitle;
		private TextView catTitle;
		private TextView achivDesc;
		private TextView achivPoints;
		private ImageView achivIcon;
	}
}