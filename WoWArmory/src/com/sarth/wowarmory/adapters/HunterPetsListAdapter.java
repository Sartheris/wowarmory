package com.sarth.wowarmory.adapters;

import java.util.ArrayList;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.mountsandpets.HunterPets;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class HunterPetsListAdapter extends ArrayAdapter<Object> {

	private LayoutInflater inflater;
	private ArrayList<Object> petsArray;
	private Context context;

	public HunterPetsListAdapter(Context context, int resource, ArrayList<Object> petsArray) {
		super(context, resource, petsArray);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.petsArray = petsArray;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		HunterPets hp = (HunterPets) petsArray.get(position);
		convertView = inflater.inflate(R.layout.item_row_hunter_pets, null);
		holder.tvName = (TextView) convertView.findViewById(R.id.tvHunterPetName);
		holder.tvRole = (TextView) convertView.findViewById(R.id.tvHunterPetSpecRole);
		holder.tvSpec = (TextView) convertView.findViewById(R.id.tvHunterPetSpecName);
		holder.tvPetFamily = (TextView) convertView.findViewById(R.id.tvHunterPetFamily);
		holder.iv = (ImageView) convertView.findViewById(R.id.ivHunterPet);
		holder.tvName.setText(hp.petName);
		holder.tvRole.setText("Role: " + hp.petRole);
		holder.tvSpec.setText("Pet Spec: " + hp.petSpecName);
		holder.tvPetFamily.setText("Pet Family: " + hp.petFamily);
		String imgUrl = String.format(context.getString(R.string.url_item_icon), Commons.INST.regionUrl, hp.petIcon);
		ImageLoader.getInstance().displayImage(imgUrl, holder.iv);
		convertView.setTag(holder);
		return convertView;
	}

	private static class ViewHolder {
		private TextView tvName, tvSpec, tvRole, tvPetFamily;
		private ImageView iv;
	}
}