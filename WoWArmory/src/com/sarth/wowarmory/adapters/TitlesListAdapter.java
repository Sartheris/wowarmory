package com.sarth.wowarmory.adapters;

import java.util.ArrayList;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TitlesListAdapter extends ArrayAdapter<String> {

	private LayoutInflater inflater;
	private ArrayList<String> titles;

	public TitlesListAdapter(Context context, int resource, ArrayList<String> titles) {
		super(context, resource, titles);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.titles = titles;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			if (position == 0) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.iv_title_item_row, null);
				holder.iv = (ImageView) convertView.findViewById(R.id.ivTitlesPortrait);
				ImageLoader.getInstance().displayImage(titles.get(0), holder.iv);
			} else {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.item_row_title, null);
				holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitleName);
				holder.tvTitle.setText(String.format(titles.get(position), Commons.INST.charName));
			}
		} else {
			if (position == 0) {
				holder = (ViewHolder) convertView.getTag();
				convertView = inflater.inflate(R.layout.iv_title_item_row, null);
				holder.iv = (ImageView) convertView.findViewById(R.id.ivTitlesPortrait);
				ImageLoader.getInstance().displayImage(titles.get(0), holder.iv);
			} else {
				holder = (ViewHolder) convertView.getTag();
				convertView = inflater.inflate(R.layout.item_row_title, null);
				holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitleName);
				holder.tvTitle.setText(String.format(titles.get(position), Commons.INST.charName));
			}
		}
		convertView.setTag(holder);
		return convertView;
	}

	private static class ViewHolder {
		private TextView tvTitle;
		private ImageView iv;
	}
}