package com.sarth.wowarmory.adapters;

import java.util.ArrayList;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.models.RealmsInfo;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RealmsStatusListAdapter extends ArrayAdapter<RealmsInfo> {

	private ArrayList<RealmsInfo> mItems;
	private LayoutInflater inflater;
	private int statusCheck;

	public RealmsStatusListAdapter(Context context, int resource, ArrayList<RealmsInfo> objects, int statusCheck) {
		super(context, resource, objects);
		this.mItems = objects;
		this.statusCheck = statusCheck;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		convertView = inflater.inflate(R.layout.item_row_spinner, null);
		holder.txtRealm = (TextView) convertView.findViewById(R.id.txt_realm);
		holder.imgStatus = (ImageView) convertView.findViewById(R.id.iv_status);
		if (statusCheck == 1) {
			if (mItems.get(position).status == true) {
				holder.imgStatus.setImageResource(R.drawable.up_green);
			} else {
				holder.imgStatus.setImageResource(R.drawable.down_red);
			}
		} else {
			holder.imgStatus.setVisibility(View.GONE);
		}
		holder.txtRealm.setText(String.valueOf(mItems.get(position).realm));
		convertView.setTag(holder);
		return convertView;
	}

	private static class ViewHolder {
		private TextView txtRealm;
		private ImageView imgStatus;
	}
}