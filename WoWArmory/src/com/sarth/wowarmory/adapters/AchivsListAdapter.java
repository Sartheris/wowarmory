package com.sarth.wowarmory.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.achievements.Achievement;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AchivsListAdapter extends ArrayAdapter<Achievement> {

	private ArrayList<Achievement> mItems;
	private LayoutInflater inflater;

	public AchivsListAdapter(Context context, int resource, ArrayList<Achievement> aa) {
		super(context, resource, aa);
		this.mItems = aa;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		Achievement aa = mItems.get(position);
		if (aa.hasAchiv.equals("1")) {
			if (convertView == null) {
				holder = new ViewHolder();
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			convertView = inflater.inflate(R.layout.achiv_element, null);
			getRowElement(holder, convertView, position, aa);
			holder.achivDate = (TextView) convertView.findViewById(R.id.tvAchivRowDate);
			holder.achivDate.setText(getFormattedDate(aa.achivDate));
		} else {
			if (convertView == null) {
				holder = new ViewHolder();
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			convertView = inflater.inflate(R.layout.achiv_missing_element, null);
			getRowElement(holder, convertView, position, aa);
		}
		convertView.setTag(holder);
		return convertView;
	}

	private String getFormattedDate(String achivDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(achivDate));
		return formatter.format(calendar.getTime());
	}

	/** Get Achievement Row Element */
	private void getRowElement(ViewHolder holder, View convertView, int position, Achievement aa) {
		holder.achivTitle = (TextView) convertView.findViewById(R.id.tvAchivRowTitle);
		holder.achivTitle.setText(String.valueOf(mItems.get(position).achivTitle));
		holder.achivDesc = (TextView) convertView.findViewById(R.id.tvAchivRowDesc);
		holder.achivDesc.setText(String.valueOf(mItems.get(position).achivDescription));
		holder.achivPoints = (TextView) convertView.findViewById(R.id.ivAchivRowPoints);
		holder.achivPoints.setText("" + mItems.get(position).achivPoints);
		String imgUrl = "http://" + Commons.INST.regionUrl + ".media.blizzard.com/wow/icons/56/" + aa.achivIcon + ".jpg";
		holder.achivIcon = (ImageView) convertView.findViewById(R.id.ivAchivRowIcon);
		ImageLoader.getInstance().displayImage(imgUrl, holder.achivIcon);
	}

	private static class ViewHolder {
		private TextView achivDate;
		private TextView achivTitle;
		private TextView achivDesc;
		private TextView achivPoints;
		private ImageView achivIcon;
	}
}