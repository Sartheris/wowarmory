package com.sarth.wowarmory.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import com.sarth.wowarmory.R;
import com.sarth.wowarmory.progression.Progression;
import com.sarth.wowarmory.progression.ProgressionBoss;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ProgressionListAdapter extends ArrayAdapter<Object> {

	private LayoutInflater inflater;
	private ArrayList<Object> progArray;
	private int position = 1;
	private TextView tv;
	private Context context;

	public ProgressionListAdapter(Context context, int resource, ArrayList<Object> progArray) {
		super(context, resource, progArray);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.progArray = progArray;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getDropDownView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Progression prog = (Progression) progArray.get(position);
		convertView = inflater.inflate(R.layout.item_row_progression, null);
		holder.tvName = (TextView) convertView.findViewById(R.id.tvProgressionRaidName);
		holder.tvNormal = (TextView) convertView.findViewById(R.id.tvProgressionNormal);
		holder.tvHeroic = (TextView) convertView.findViewById(R.id.tvProgressionHeroic);
		holder.ll = (LinearLayout) convertView.findViewById(R.id.llBosses);
		holder.tvName.setText(prog.raidName);
		holder.tvNormal.setText("Normal: " + prog.normal);
		holder.tvHeroic.setText("Heroic: " + prog.heroic);
		for (int i = 0; i < prog.boss.size(); i++) {
			holder.ll.addView(addNewRelativeLayout(prog.boss.get(i)));
		}
		convertView.setTag(holder);
		return convertView;
	}

	private RelativeLayout addNewRelativeLayout(ProgressionBoss boss) {
		RelativeLayout rl = new RelativeLayout(context);
		rl.setLayoutParams(new LayoutParams(android.widget.RelativeLayout.LayoutParams.MATCH_PARENT, android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT));
		rl.setBackground(context.getResources().getDrawable(R.drawable.feed_item_row));
		rl.setPadding(10, 10, 10, 10);
		tv = new TextView(context);
		tv.setTextColor(context.getResources().getColor(R.color.common));
		tv.setTextSize(18);
		tv.setText(boss.bossName);
		tv.setId(position);
		position++;
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		tv.setLayoutParams(lp);
		rl.addView(tv);
		if (boss.normalTimestamp != null) {
			getTextViews("Normal Kills: ", boss.normalKills, boss.normalTimestamp, rl);
		}
		if (boss.heroicTimestamp != null) {
			getTextViews("Heroic Kills: ", boss.heroicKills, boss.heroicTimestamp, rl);
		}
		if (boss.lfrTimestamp != null) {
			getTextViews("LFR Kills: ", boss.lfrKills, boss.lfrTimestamp, rl);
		}
		if (boss.flexTimestamp != null) {
			getTextViews("Flexible Kills: ", boss.flexKills, boss.flexTimestamp, rl);
		}
		return rl;
	}

	private void getTextViews(String killType, int kills, String flexTimestamp, RelativeLayout rl) {
		RelativeLayout.LayoutParams lp;
		tv = new TextView(context);
		tv.setTextColor(context.getResources().getColor(R.color.common));
		tv.setText(killType + kills);
		tv.setId(position);
		lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		lp.addRule(RelativeLayout.BELOW, position - 1);
		tv.setLayoutParams(lp);
		rl.addView(tv);

		tv = new TextView(context);
		tv.setTextColor(context.getResources().getColor(R.color.common));
		tv.setText(getFeedDate(flexTimestamp));
		lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		lp.addRule(RelativeLayout.BELOW, position - 1);
		position++;
		tv.setLayoutParams(lp);
		rl.addView(tv);
	}

	private String getFeedDate(String time) {
		long i = Long.parseLong(time);
		if (i > 0) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(Long.parseLong(time));
			return formatter.format(calendar.getTime());
		} else {
			return "";
		}
	}

	private static class ViewHolder {
		private TextView tvName, tvNormal, tvHeroic;
		private LinearLayout ll;
	}
}