package com.sarth.wowarmory.adapters;

import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.Fragment1;
import com.sarth.wowarmory.Fragment10;
import com.sarth.wowarmory.Fragment11;
import com.sarth.wowarmory.Fragment12;
import com.sarth.wowarmory.Fragment13;
import com.sarth.wowarmory.Fragment2;
import com.sarth.wowarmory.Fragment3;
import com.sarth.wowarmory.Fragment4;
import com.sarth.wowarmory.Fragment5;
import com.sarth.wowarmory.Fragment6;
import com.sarth.wowarmory.Fragment7;
import com.sarth.wowarmory.Fragment8;
import com.sarth.wowarmory.Fragment9;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FragmentTabsAdapter extends FragmentPagerAdapter {

	public FragmentTabsAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment;
		switch (position) {
		case 0:
			fragment = new Fragment1();
			break;
		case 1:
			fragment = new Fragment2();
			break;
		case 2:
			fragment = new Fragment3();
			break;
		case 3:
			fragment = new Fragment4();
			break;
		case 4:
			fragment = new Fragment5();
			break;
		case 5:
			fragment = new Fragment6();
			break;
		case 6:
			fragment = new Fragment7();
			break;
		case 7:
			fragment = new Fragment8();
			break;
		case 8:
			fragment = new Fragment9();
			break;
		case 9:
			fragment = new Fragment10();
			break;
		case 10:
			fragment = new Fragment11();
			break;
		case 11:
			fragment = new Fragment12();
			break;
		case 12:
			// Hunter pets case
			if (Commons.INST.charClassId == 3) {
				fragment = new Fragment13();
			} else {
				return null;
			}
			break;
		default:
			fragment = null;
			break;
		}
		return fragment;
	}
	
	

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return ("Character");
		case 1:
			return ("Equipped Items");
		case 2:
			return ("Talents");
		case 3:
			return ("Achievements");
		case 4:
			return ("Professions");
		case 5:
			return ("Titles");
		case 6:
			return ("PvP");
		case 7:
			return ("Activity Feed");
		case 8:
			return ("Reputation");
		case 9:
			return ("Progression");
		case 10:
			return ("Mounts");
		case 11:
			return ("Pets");
		case 12:
			if (Commons.INST.charClassId == 3) {
				return ("Hunter Pets");
			} else {
				return null;
			}
		}
		return null;
	}

	@Override
	public int getCount() {
		if (Commons.INST.charClassId == 3) {
			return 13;
		} else {
			return 12;
		}
	}
}