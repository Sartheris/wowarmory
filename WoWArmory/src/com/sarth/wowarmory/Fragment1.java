package com.sarth.wowarmory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;

public class Fragment1 extends Fragment implements JSONParserCallback {

	private LinearLayout layout;
	private JSONGet taskGet;
	private TextView tvCharName;
	private TextView tvStatBaseStrength, tvStatBaseAgility, tvStatBaseStamina, tvStatBaseIntellect, tvStatBaseSpirit, tvStatBaseMastery;
	private TextView tvStatMeleeDamage, tvStatMeleeDPS, tvStatMeleePower, tvStatMeleeSpeed, tvStatMeleeHaste, tvStatMeleeCrit;
	private TextView tvStatRangedDamage, tvStatRangedDPS, tvStatRangedPower, tvStatRangedSpeed, tvStatRangedHaste, tvStatRangedCrit;
	private TextView tvStatSpellPower, tvStatSpellHaste, tvStatSpellCrit, tvStatSpellManaRegen, tvStatSpellCombatRegen;
	private TextView tvStatDefenseArmor, tvStatDefenseDodge, tvStatDefenseParry, tvStatDefenseBlock, tvStatDefenseResilience, tvStatDefensePvpPower;
	private ImageView iv;
	private View v;

	public Fragment1() {
		// Empty constructor
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (menuVisible && !Commons.INST.fragment1done) {
			String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
					+ "?fields=guild" + "&locale=" + Commons.INST.locale;
			taskGet = new JSONGet(this, adres);
			taskGet.execute();
		} else {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment1, container, false);
		v = rootView;
		initializeViews(rootView);
		if (!Commons.INST.fragment1done) {
			createViews();
		}
		return rootView;
	}

	private void createViews() {
		tvCharName.setText(Commons.INST.charName + " ");
		String charInfo = "Level " + String.valueOf(Commons.INST.charLevel + " ");
		addTextView(charInfo);
		charInfo = Commons.INST.charRace + " ";
		addTextView(charInfo);
		charInfo = Commons.INST.charClass + " ";
		addTextView(charInfo);
		String thumbnailId = Commons.INST.charThumbnail.replace(Commons.INST.realmSlug, "").replace("-avatar.jpg", "");
		String imgUrl = "http://" + Commons.INST.regionUrl + ".battle.net/static-render/" + Commons.INST.regionUrl + "/" + Commons.INST.realmSlug + thumbnailId
				+ "-inset.jpg";
		ImageLoader.getInstance().displayImage(imgUrl, iv);
		setImageViewClick(iv);
		try {
			JSONObject jsonGet = new JSONObject(getActivity().getIntent().getExtras().getString("json"));
			JSONObject j = jsonGet.getJSONObject("stats");

			tvStatBaseStrength.setText(j.getString("str"));
			tvStatBaseAgility.setText(j.getString("agi"));
			tvStatBaseStamina.setText(j.getString("sta"));
			tvStatBaseIntellect.setText(j.getString("int"));
			tvStatBaseSpirit.setText(j.getString("spr"));
			tvStatBaseMastery.setText(getFormattedDecimal(j.getDouble("mastery")) + "%");

			tvStatMeleeDamage.setText(j.getString("mainHandDmgMin") + "-" + j.getString("mainHandDmgMax"));
			tvStatMeleeSpeed.setText(getFormattedDecimal(j.getDouble("mainHandSpeed")) + " / " + getFormattedDecimal(j.getDouble("offHandSpeed")));
			String mainDps = getFormattedDecimal(j.getDouble("mainHandDps"));
			String offDps = getFormattedDecimal(j.getDouble("offHandDps"));
			tvStatMeleeDPS.setText(mainDps + "/" + offDps);
			tvStatMeleeHaste.setText(getStatString(getFormattedDecimal(j.getDouble("haste"))) + "%");
			tvStatMeleePower.setText(j.getString("attackPower"));
			tvStatMeleeCrit.setText(getStatString(getFormattedDecimal(j.getDouble("crit"))) + "%");

			tvStatSpellCombatRegen.setText(j.getInt("mana5Combat") + "");
			tvStatSpellCrit.setText(getStatString(getFormattedDecimal(j.getDouble("spellCrit"))) + "%");
			tvStatSpellHaste.setText(getStatString(getFormattedDecimal(j.getDouble("spellHaste"))) + "%");
			tvStatSpellManaRegen.setText(j.getInt("mana5") + "");
			tvStatSpellPower.setText(j.getInt("spellPower") + "");

			tvStatRangedDamage.setText(getStat(j.getInt("rangedDmgMin")) + " - " + getStat(j.getInt("rangedDmgMax")));
			tvStatRangedDPS.setText(getStatString(getFormattedDecimal(j.getDouble("rangedDps"))));
			tvStatRangedCrit.setText(getStatString(getFormattedDecimal(j.getDouble("rangedCrit"))) + "%");
			tvStatRangedHaste.setText(getFormattedDecimal(j.getDouble("rangedHaste")) + "%");
			tvStatRangedPower.setText(j.getInt("rangedAttackPower") + "");
			tvStatRangedSpeed.setText(getStatString(getFormattedDecimal(j.getDouble("rangedSpeed"))));

			tvStatDefenseArmor.setText(j.getInt("armor") + "");
			tvStatDefenseBlock.setText(getStatString(getFormattedDecimal(j.getDouble("block"))) + "%");
			tvStatDefenseDodge.setText(getStatString(getFormattedDecimal(j.getDouble("dodge"))) + "%");
			tvStatDefenseParry.setText(getStatString(getFormattedDecimal(j.getDouble("parry"))) + "%");
			tvStatDefensePvpPower.setText(getStatString(getFormattedDecimal(j.getDouble("pvpPower"))) + "%");
			tvStatDefenseResilience.setText(getStatString(getFormattedDecimal(j.getDouble("pvpResilience"))) + "%");

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private String getStatString(String formattedDecimal) {
		if (formattedDecimal.equals("-1")) {
			return "--";
		} else {
			return formattedDecimal + "";
		}
	}

	private String getStat(int stat) {
		if (stat == -1) {
			return "--";
		} else {
			return stat + "";
		}
	}

	private String getFormattedDecimal(double d) {
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
		dfs.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("#.##", dfs);
		return df.format(d);
	}

	private void addTextView(String s) {
		TextView tv = new TextView(getActivity());
		tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		tv.setText(s);
		tv.setTextColor(getResources().getColor(R.color.itemlevel));
		layout.addView(tv);
	}

	private void setImageViewClick(ImageView iv2) {
		iv2.setOnLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				Vibrator vib = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
				vib.vibrate(50);
				AlertDialog.Builder db = new AlertDialog.Builder(getActivity());
				db.setNegativeButton("Save Image", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						BitmapDrawable drawable = (BitmapDrawable) iv.getDrawable();
						Bitmap bitmap = drawable.getBitmap();
						File sdCardDirectory = Environment.getExternalStorageDirectory();
						File image = new File(sdCardDirectory, "wowcharportrait" + System.currentTimeMillis() + ".png");
						boolean success = false;
						try {
							FileOutputStream outStream = new FileOutputStream(image);
							bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
							outStream.flush();
							outStream.close();
							success = true;
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						if (success) {
							Toast.makeText(getActivity(), "Image saved to SD Card", Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(getActivity(), "Error during image saving", Toast.LENGTH_LONG).show();
						}
					}
				});
				db.setNeutralButton("Exit", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				AlertDialog dlg = db.create();
				dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dlg.show();
				return false;
			}
		});
		Commons.INST.fragment1done = true;
	}

	private void initializeViews(View rootView) {
		layout = (LinearLayout) rootView.findViewById(R.id.layout);
		iv = (ImageView) rootView.findViewById(R.id.ivPortrait);
		tvCharName = (TextView) rootView.findViewById(R.id.tvfragment1CharName);
		tvStatBaseStrength = (TextView) rootView.findViewById(R.id.tvStatBaseStrength);
		tvStatBaseAgility = (TextView) rootView.findViewById(R.id.tvStatBaseAgility);
		tvStatBaseStamina = (TextView) rootView.findViewById(R.id.tvStatBaseStamina);
		tvStatBaseIntellect = (TextView) rootView.findViewById(R.id.tvStatBaseIntellect);
		tvStatBaseSpirit = (TextView) rootView.findViewById(R.id.tvStatBaseSpirit);
		tvStatBaseMastery = (TextView) rootView.findViewById(R.id.tvStatBaseMastery);

		tvStatMeleeDamage = (TextView) rootView.findViewById(R.id.tvStatMeleeDamage);
		tvStatMeleeDPS = (TextView) rootView.findViewById(R.id.tvStatMeleeDPS);
		tvStatMeleeSpeed = (TextView) rootView.findViewById(R.id.tvStatMeleeSpeed);
		tvStatMeleeHaste = (TextView) rootView.findViewById(R.id.tvStatMeleeHaste);
		tvStatMeleePower = (TextView) rootView.findViewById(R.id.tvStatMeleePower);
		tvStatMeleeCrit = (TextView) rootView.findViewById(R.id.tvStatMeleeCrit);

		tvStatRangedDamage = (TextView) rootView.findViewById(R.id.tvStatRangedDamage);
		tvStatRangedCrit = (TextView) rootView.findViewById(R.id.tvStatRangedCrit);
		tvStatRangedDPS = (TextView) rootView.findViewById(R.id.tvStatRangedDPS);
		tvStatRangedHaste = (TextView) rootView.findViewById(R.id.tvStatRangedHaste);
		tvStatRangedPower = (TextView) rootView.findViewById(R.id.tvStatRangedPower);
		tvStatRangedSpeed = (TextView) rootView.findViewById(R.id.tvStatRangedSpeed);

		tvStatSpellCombatRegen = (TextView) rootView.findViewById(R.id.tvStatSpellCombatRegen);
		tvStatSpellCrit = (TextView) rootView.findViewById(R.id.tvStatSpellCrit);
		tvStatSpellHaste = (TextView) rootView.findViewById(R.id.tvStatSpellHaste);
		tvStatSpellManaRegen = (TextView) rootView.findViewById(R.id.tvStatSpellManaRegen);
		tvStatSpellPower = (TextView) rootView.findViewById(R.id.tvStatSpellPower);

		tvStatDefenseArmor = (TextView) rootView.findViewById(R.id.tvStatDefenseArmor);
		tvStatDefenseBlock = (TextView) rootView.findViewById(R.id.tvStatDefenseBlock);
		tvStatDefenseDodge = (TextView) rootView.findViewById(R.id.tvStatDefenseDodge);
		tvStatDefenseParry = (TextView) rootView.findViewById(R.id.tvStatDefenseParry);
		tvStatDefensePvpPower = (TextView) rootView.findViewById(R.id.tvStatDefensePvPPower);
		tvStatDefenseResilience = (TextView) rootView.findViewById(R.id.tvStatDefenseResilience);
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		try {
			JSONObject j = jsonGet.getJSONObject("guild");
			LinearLayout ll = (LinearLayout) v.findViewById(R.id.llGuildInfo);
			if (j.has("name")) {
				ll.setVisibility(View.VISIBLE);
				TextView tvGuildName = (TextView) v.findViewById(R.id.tvGuildName);
				TextView tvGuildLevel = (TextView) v.findViewById(R.id.tvGuildLevel);
				TextView tvGuildMembers = (TextView) v.findViewById(R.id.tvGuildMembers);
				TextView tvGuildAchivs = (TextView) v.findViewById(R.id.tvGuildAchievementPoints);
				tvGuildName.setText("<" + j.getString("name") + ">");
				tvGuildLevel.setText(j.getInt("level") + " Level");
				tvGuildMembers.setText(j.getInt("members") + " Members");
				tvGuildAchivs.setText("Achievement Points: " + j.getInt("achievementPoints"));
			} else {
				ll.setVisibility(View.GONE);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}