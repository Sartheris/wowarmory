package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.adapters.HunterPetsListAdapter;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.mountsandpets.HunterPets;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class Fragment13 extends Fragment implements JSONParserCallback {

	private LoadingDialogFragment loadingFragment;
	private JSONGet taskGet;
	private ListView lv;

	public Fragment13() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment13, container, false);
		lv = (ListView) rootView.findViewById(R.id.lvHunterPets);
		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (!menuVisible) {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
			if (loadingFragment != null) {
				loadingFragment.dismiss();
			}
		} else {
			if (!Commons.INST.fragment13done) {
				loadingFragment = LoadingDialogFragment.newInstance("Getting hunter pets...");
				loadingFragment.show(getActivity().getSupportFragmentManager(), "tag");
				String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
						+ "?fields=hunterPets" + "&locale=" + Commons.INST.locale;
				taskGet = new JSONGet(this, adres);
				taskGet.execute();
			}
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		try {
			JSONArray jArr = jsonGet.getJSONArray("hunterPets");
			ArrayList<Object> arr = new ArrayList<Object>();
			for (int i = 0; i < jArr.length(); i++) {
				JSONObject j = jArr.getJSONObject(i);
				HunterPets hp = new HunterPets();
				hp.petName = j.getString("name");
				hp.petIcon = j.getJSONObject("spec").getString("icon");
				hp.petRole = j.getJSONObject("spec").getString("role");
				hp.petSpecName = j.getJSONObject("spec").getString("name");
				hp.petFamily = j.getString("familyName");
				arr.add(hp);
			}
			HunterPetsListAdapter adap = new HunterPetsListAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, arr);
			lv.setAdapter(adap);
			loadingFragment.dismiss();
			Commons.INST.fragment13done = true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}