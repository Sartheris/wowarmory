package com.sarth.wowarmory;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.app.Activity;
import android.content.Intent;

public class SplashScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		Commons.INST.locale = Locale.getDefault().toString();
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheOnDisc(true).displayer(new FadeInBitmapDisplayer(300))
				.imageScaleType(ImageScaleType.EXACTLY).build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).defaultDisplayImageOptions(defaultOptions).build();
		ImageLoader.getInstance().init(config);
		final Timer tt = new Timer();
		tt.schedule(new TimerTask() {

			@Override
			public void run() {
				startActivity(new Intent(SplashScreen.this, MainActivity.class));
				finish();
			}
		}, 3000);
		LinearLayout ll = (LinearLayout) findViewById(R.id.introfon);
		ll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				tt.cancel();
				startActivity(new Intent(SplashScreen.this, MainActivity.class));
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}
}