package com.sarth.wowarmory.sqlite;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.sarth.wowarmory.Commons;
import com.sarth.wowarmory.achievements.Achievement;
import com.sarth.wowarmory.achievements.AchievementCategory;
import com.sarth.wowarmory.achievements.CurrentCharacterAchievs;
import com.sarth.wowarmory.models.ClassInfo;
import com.sarth.wowarmory.models.RaceInfo;
import com.sarth.wowarmory.models.RealmsInfo;

public class DataHelper {

	private DataModel dbModel;

	public DataHelper(Context context) {
		dbModel = new DataModel(context);
	}

	public ArrayList<RealmsInfo> selectAllRealms(String regionUrl) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.query(DataSchema.REALMS_TABLE_NAME, new String[] { DataSchema.RealmsColumns._ID, DataSchema.RealmsColumns.REALM_REGION,
				DataSchema.RealmsColumns.REALM_NAME, DataSchema.RealmsColumns.REALM_SLUG }, "realm_region = '" + regionUrl + "'", null, null, null, null);
		c.moveToFirst();
		ArrayList<RealmsInfo> ri = new ArrayList<RealmsInfo>();
		while (c.isAfterLast() == false) {
			Long id = c.getLong(c.getColumnIndex(DataSchema.RealmsColumns._ID));
			String rregion = c.getString(c.getColumnIndex(DataSchema.RealmsColumns.REALM_REGION));
			String rname = c.getString(c.getColumnIndex(DataSchema.RealmsColumns.REALM_NAME));
			String rslug = c.getString(c.getColumnIndex(DataSchema.RealmsColumns.REALM_SLUG));
			ri.add(new RealmsInfo(id, rregion, rname, rslug));
			c.moveToNext();
		}
		c.close();
		return ri;
	}

	public boolean hasClasses() {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.query(DataSchema.CLASSES_TABLE_NAME, new String[] { DataSchema.ClassesColumns._ID }, "", null, null, null, null);
		if (!(c.moveToFirst()) || c.getCount() == 0) {
			c.close();
			return false;
		} else {
			c.close();
			return true;
		}
	}

	public void insertClasses(ArrayList<ClassInfo> classes) {
		for (int j = 0; j < classes.size(); j++) {
			ContentValues values = new ContentValues();
			values.put(DataSchema.ClassesColumns.CLASS_ID, classes.get(j).class_id);
			values.put(DataSchema.ClassesColumns.CLASS_NAME, classes.get(j).name);
			values.put(DataSchema.ClassesColumns.CLASS_POWERTYPE, classes.get(j).powerType);
			SQLiteDatabase db = dbModel.getReadableDatabase();
			Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.CLASSES_TABLE_NAME + " WHERE " + DataSchema.ClassesColumns.CLASS_ID + " = ?" + " AND "
					+ DataSchema.ClassesColumns.CLASS_NAME + " = ?", new String[] { classes.get(j).powerType, classes.get(j).name });
			if (c.getCount() == 0) {
				db.insert(DataSchema.CLASSES_TABLE_NAME, null, values);
			} else {
				db.update(DataSchema.CLASSES_TABLE_NAME, values, DataSchema.ClassesColumns.CLASS_ID + " = ?", new String[] { classes.get(j).name });
			}
			c.close();
		}
	}

	public void getClass(int clas) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.query(DataSchema.CLASSES_TABLE_NAME, new String[] { DataSchema.ClassesColumns._ID, DataSchema.ClassesColumns.CLASS_ID,
				DataSchema.ClassesColumns.CLASS_NAME, DataSchema.ClassesColumns.CLASS_POWERTYPE }, "class_id = '" + clas + "'", null, null, null, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			Commons.INST.charClass = c.getString(c.getColumnIndex(DataSchema.ClassesColumns.CLASS_NAME));
			Commons.INST.charPowerType = c.getString(c.getColumnIndex(DataSchema.ClassesColumns.CLASS_POWERTYPE));
			c.moveToNext();
		}
		c.close();
	}

	public boolean hasRaces() {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.query(DataSchema.RACES_TABLE_NAME, new String[] { DataSchema.RacesColumns._ID }, "", null, null, null, null);
		if (!(c.moveToFirst()) || c.getCount() == 0) {
			c.close();
			return false;
		} else {
			c.close();
			return true;
		}
	}

	public void insertRaces(ArrayList<RaceInfo> races) {
		for (int j = 0; j < races.size(); j++) {
			ContentValues values = new ContentValues();
			values.put(DataSchema.RacesColumns.RACE_ID, races.get(j).race_id);
			values.put(DataSchema.RacesColumns.RACE_NAME, races.get(j).race_name);
			values.put(DataSchema.RacesColumns.RACE_SIDE, races.get(j).race_side);
			SQLiteDatabase db = dbModel.getReadableDatabase();
			Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.RACES_TABLE_NAME + " WHERE " + DataSchema.RacesColumns.RACE_ID + " = ?" + " AND "
					+ DataSchema.RacesColumns.RACE_NAME + " = ?", new String[] { races.get(j).race_name, races.get(j).race_side });
			if (c.getCount() == 0) {
				db.insert(DataSchema.RACES_TABLE_NAME, null, values);
			} else {
				db.update(DataSchema.RACES_TABLE_NAME, values, DataSchema.RacesColumns.RACE_ID + " = ?", new String[] { races.get(j).race_name });
			}
			c.close();
		}
	}

	public void getRace(int race) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.query(DataSchema.RACES_TABLE_NAME, new String[] { DataSchema.RacesColumns._ID, DataSchema.RacesColumns.RACE_ID,
				DataSchema.RacesColumns.RACE_NAME, DataSchema.RacesColumns.RACE_SIDE }, "race_id = '" + race + "'", null, null, null, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			Commons.INST.charRace = c.getString(c.getColumnIndex(DataSchema.RacesColumns.RACE_NAME));
			Commons.INST.charSide = c.getString(c.getColumnIndex(DataSchema.RacesColumns.RACE_SIDE));
			c.moveToNext();
		}
		c.close();
	}

	public void insertAchievements(ArrayList<AchievementCategory> categories) {
		try {
			for (int j = 0; j < categories.size(); j++) {
				insertAchCategory(categories.get(j), 0);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void insertAchCategory(AchievementCategory cat, long parentID) {
		ContentValues values = new ContentValues();
		values.put(DataSchema.AchievementCategoriesColumns.CATEGORY_ID, cat.CategoryId);
		values.put(DataSchema.AchievementCategoriesColumns.CATEGORY_NAME, cat.CategoryName);
		values.put(DataSchema.AchievementCategoriesColumns.PARENT_ID, parentID);
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT " + DataSchema.AchievementCategoriesColumns._ID + " FROM " + DataSchema.ACHIEVEMENTS_CATEGORIES_TABLE_NAME + " WHERE "
				+ DataSchema.AchievementCategoriesColumns.CATEGORY_ID + " = ?", new String[] { "" + cat.CategoryId });
		long lastID = 0;
		if (c.getCount() == 0) {
			lastID = db.insert(DataSchema.ACHIEVEMENTS_CATEGORIES_TABLE_NAME, null, values);
		} else {
			lastID = c.getLong(c.getColumnIndex(DataSchema.AchievementCategoriesColumns._ID));
			db.update(DataSchema.ACHIEVEMENTS_CATEGORIES_TABLE_NAME, values, DataSchema.AchievementCategoriesColumns.CATEGORY_ID + " = ?", new String[] { ""
					+ cat.CategoryId });
		}
		c.close();
		if (cat.SubCategories != null && cat.SubCategories.size() > 0) {
			for (int j = 0; j < cat.SubCategories.size(); j++) {
				insertAchCategory(cat.SubCategories.get(j), lastID);
			}
		}
		if (cat.Achievements != null && cat.Achievements.size() > 0) {
			for (int j = 0; j < cat.Achievements.size(); j++) {
				insertAchAchievement(cat.Achievements.get(j), lastID);
			}
		}
	}

	/** Achievements **/

	private void insertAchAchievement(Achievement ach, long categoryID) {
		ContentValues values = new ContentValues();
		values.put(DataSchema.AchievementsColumns.CATEGORY_ID, categoryID);
		values.put(DataSchema.AchievementsColumns.ACHIEVEMENT_ID, ach.achivId);
		values.put(DataSchema.AchievementsColumns.ACHIEVEMENT_TITLE, ach.achivTitle);
		values.put(DataSchema.AchievementsColumns.ACHIEVEMENT_DESCRIPTION, ach.achivDescription);
		values.put(DataSchema.AchievementsColumns.ACHIEVEMENT_ICON, ach.achivIcon);
		values.put(DataSchema.AchievementsColumns.ACHIEVEMENT_POINTS, ach.achivPoints);
		values.put(DataSchema.AchievementsColumns.ACHIEVEMENT_TITLE, ach.achivTitle);
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.ACHIEVEMENTS_TABLE_NAME + " WHERE " + DataSchema.AchievementsColumns.ACHIEVEMENT_ID + " = ?",
				new String[] { "" + ach.achivId });
		if (c.getCount() == 0) {
			db.insert(DataSchema.ACHIEVEMENTS_TABLE_NAME, null, values);
		} else {
			db.update(DataSchema.ACHIEVEMENTS_TABLE_NAME, values, DataSchema.AchievementsColumns.ACHIEVEMENT_ID + " = ?", new String[] { "" + ach.achivId });
		}
		c.close();
	}

	/** Achievement Categories */
	public ArrayList<AchievementCategory> selectCategories(long i) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		ArrayList<AchievementCategory> ac = new ArrayList<AchievementCategory>();
		Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.ACHIEVEMENTS_CATEGORIES_TABLE_NAME + " WHERE " + DataSchema.AchievementCategoriesColumns.PARENT_ID
				+ " = " + i, null);
		if ((c.moveToFirst())) {
			do {
				String name = c.getString(c.getColumnIndex(DataSchema.AchievementCategoriesColumns.CATEGORY_NAME));
				long id = c.getLong(c.getColumnIndex(DataSchema.AchievementCategoriesColumns._ID));
				int categoryId = c.getInt(c.getColumnIndex(DataSchema.AchievementCategoriesColumns.CATEGORY_ID));
				int parentId = c.getInt(c.getColumnIndex(DataSchema.AchievementCategoriesColumns.PARENT_ID));
				ac.add(new AchievementCategory(name, id, categoryId, parentId));
			} while (c.moveToNext());
			c.close();
		}
		return ac;
	}

	/** Update Achievements for Current Character */
	public void updateCurrentCharacterAchivs(ArrayList<CurrentCharacterAchievs> cca) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		db.execSQL("UPDATE " + DataSchema.ACHIEVEMENTS_TABLE_NAME + " SET has_achiv = 0");
		db.execSQL("UPDATE " + DataSchema.ACHIEVEMENTS_TABLE_NAME + " SET achiv_date = 0");
		String ids = "";
		for (int j = 0; j < cca.size(); j++) {
			ids += cca.get(j).achivId + ",";
			db.execSQL("UPDATE " + DataSchema.ACHIEVEMENTS_TABLE_NAME + " SET achiv_date = " + cca.get(j).achivDate + " WHERE "
					+ DataSchema.AchievementsColumns.ACHIEVEMENT_ID + " = " + cca.get(j).achivId);
		}
		db.execSQL("UPDATE " + DataSchema.ACHIEVEMENTS_TABLE_NAME + " SET has_achiv = 1 WHERE " + DataSchema.AchievementsColumns.ACHIEVEMENT_ID + " IN ("
				+ ids.substring(0, ids.length() - 1) + ")");
	}

	public ArrayList<Achievement> selectAchivs(long i) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		ArrayList<Achievement> a = new ArrayList<Achievement>();
		Cursor c = db
				.rawQuery("SELECT * FROM " + DataSchema.ACHIEVEMENTS_TABLE_NAME + " WHERE " + DataSchema.AchievementsColumns.CATEGORY_ID + " = " + i, null);
		if ((c.moveToFirst())) {
			do {
				String achivDate = c.getString(c.getColumnIndex(DataSchema.AchievementsColumns.ACHIV_DATE));
				String hasAchiv = c.getString(c.getColumnIndex(DataSchema.AchievementsColumns.HAS_ACHIEVEMENT));
				String title = c.getString(c.getColumnIndex(DataSchema.AchievementsColumns.ACHIEVEMENT_TITLE));
				String description = c.getString(c.getColumnIndex(DataSchema.AchievementsColumns.ACHIEVEMENT_DESCRIPTION));
				String icon = c.getString(c.getColumnIndex(DataSchema.AchievementsColumns.ACHIEVEMENT_ICON));
				int points = c.getInt(c.getColumnIndex(DataSchema.AchievementsColumns.ACHIEVEMENT_POINTS));
				a.add(new Achievement(title, icon, description, points, hasAchiv, achivDate));
			} while (c.moveToNext());
			c.close();
		}
		return a;
	}

	public boolean hasAchivs() {
		boolean hasAchivs = false;
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.query(DataSchema.ACHIEVEMENTS_TABLE_NAME, new String[] { DataSchema.AchievementsColumns._ID }, "", null, null, null, null);
		if (!(c.moveToFirst()) || c.getCount() == 0) {
			hasAchivs = false;
		} else {
			hasAchivs = true;
		}
		c.close();
		return hasAchivs;
	}

	// Check for categories
	public boolean hasCategories(long parentId) {
		boolean hasSubCategories = false;
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.ACHIEVEMENTS_CATEGORIES_TABLE_NAME + " WHERE " + DataSchema.AchievementCategoriesColumns.PARENT_ID
				+ " = " + parentId, null);
		if (!(c.moveToFirst()) || c.getCount() == 0) {
			hasSubCategories = false;
		} else {
			hasSubCategories = true;
		}
		c.close();
		return hasSubCategories;
	}
}