package com.sarth.wowarmory.sqlite;

import android.provider.BaseColumns;

public class DataSchema {

	public static final String REALMS_TABLE_NAME = "realms";
	public static final String CLASSES_TABLE_NAME = "classes";
	public static final String RACES_TABLE_NAME = "races";
	public static final String ACHIEVEMENTS_CATEGORIES_TABLE_NAME = "achivs_categories";
	public static final String ACHIEVEMENTS_TABLE_NAME = "achivs";

	public static final class RealmsColumns implements BaseColumns {

		private RealmsColumns() {
			// Empty constructor
		}

		public static final String REALM_REGION = "realm_region";
		public static final String REALM_NAME = "realm_name";
		public static final String REALM_SLUG = "realm_slug";
	}

	public static final class ClassesColumns implements BaseColumns {

		private ClassesColumns() {
			// Empty constructor
		}

		public static final String CLASS_ID = "class_id";
		public static final String CLASS_NAME = "class_name";
		public static final String CLASS_POWERTYPE = "class_powertype";
	}

	public static final class RacesColumns implements BaseColumns {

		private RacesColumns() {
			// Empty constructor
		}

		public static final String RACE_ID = "race_id";
		public static final String RACE_NAME = "race_name";
		public static final String RACE_SIDE = "race_side";
	}

	public static final class AchievementCategoriesColumns implements BaseColumns {

		private AchievementCategoriesColumns() {
			// Empty constructor
		}

		public static final String CATEGORY_ID = "category_id";
		public static final String CATEGORY_NAME = "category_name";
		public static final String PARENT_ID = "parent_id";
	}

	public static final class AchievementsColumns implements BaseColumns {

		private AchievementsColumns() {
			// Empty constructor
		}

		public static final String ACHIEVEMENT_ID = "achiv_id";
		public static final String HAS_ACHIEVEMENT = "has_achiv";
		public static final String ACHIV_DATE = "achiv_date";
		public static final String ACHIEVEMENT_TITLE = "achiv_title";
		public static final String ACHIEVEMENT_ICON = "achiv_icon";
		public static final String ACHIEVEMENT_POINTS = "achiv_points";
		public static final String ACHIEVEMENT_DESCRIPTION = "achiv_description";
		public static final String CATEGORY_ID = "category_id";
	}
}