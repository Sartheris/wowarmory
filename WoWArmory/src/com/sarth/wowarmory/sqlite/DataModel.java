package com.sarth.wowarmory.sqlite;

import com.sarth.wowarmory.sqlite.DataSchema.AchievementCategoriesColumns;
import com.sarth.wowarmory.sqlite.DataSchema.AchievementsColumns;
import com.sarth.wowarmory.sqlite.DataSchema.ClassesColumns;
import com.sarth.wowarmory.sqlite.DataSchema.RacesColumns;
import com.sarth.wowarmory.sqlite.DataSchema.RealmsColumns;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataModel extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "WoWArmory";

	private static final String REALMS_CREATE_TABLE = "CREATE TABLE " + DataSchema.REALMS_TABLE_NAME + " (" + RealmsColumns._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + RealmsColumns.REALM_REGION + " VARCHAR, " + RealmsColumns.REALM_NAME + " VARCHAR, "
			+ RealmsColumns.REALM_SLUG + " VARCHAR);";

	private static final String CLASSES_CREATE_TABLE = "CREATE TABLE " + DataSchema.CLASSES_TABLE_NAME + " (" + ClassesColumns._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + ClassesColumns.CLASS_ID + " INTEGER, " + ClassesColumns.CLASS_NAME + " VARCHAR, "
			+ ClassesColumns.CLASS_POWERTYPE + " VARCHAR);";

	private static final String RACES_CREATE_TABLE = "CREATE TABLE " + DataSchema.RACES_TABLE_NAME + " (" + RacesColumns._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + RacesColumns.RACE_ID + " INTEGER, " + RacesColumns.RACE_NAME + " VARCHAR, "
			+ RacesColumns.RACE_SIDE + " VARCHAR);";

	private static final String ACHIEVEMENTS_CATEGORIES_CREATE_TABLE = "CREATE TABLE " + DataSchema.ACHIEVEMENTS_CATEGORIES_TABLE_NAME
			+ " (" + AchievementCategoriesColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + AchievementCategoriesColumns.CATEGORY_ID
			+ " INTEGER, " + AchievementCategoriesColumns.CATEGORY_NAME + " VARCHAR, " + AchievementCategoriesColumns.PARENT_ID
			+ " INTEGER);";

	private static final String ACHIEVEMENTS_CREATE_TABLE = "CREATE TABLE " + DataSchema.ACHIEVEMENTS_TABLE_NAME + " ("
			+ AchievementsColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + AchievementsColumns.CATEGORY_ID + " INTEGER, "
			+ AchievementsColumns.ACHIEVEMENT_DESCRIPTION + " VARCHAR, " + AchievementsColumns.ACHIEVEMENT_ICON + " VARCHAR, "
			+ AchievementsColumns.ACHIEVEMENT_POINTS + " INTEGER, " + AchievementsColumns.ACHIEVEMENT_TITLE + " VARCHAR, "
			+ AchievementsColumns.ACHIV_DATE + " VARCHAR, " + AchievementsColumns.HAS_ACHIEVEMENT + " INTEGER, "
			+ AchievementsColumns.ACHIEVEMENT_ID + " INTEGER);";

	public DataModel(Context c) {
		super(c, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("PRAGMA foreign_keys = ON;");
		db.execSQL(REALMS_CREATE_TABLE);
		db.execSQL(CLASSES_CREATE_TABLE);
		db.execSQL(RACES_CREATE_TABLE);
		db.execSQL(ACHIEVEMENTS_CATEGORIES_CREATE_TABLE);
		db.execSQL(ACHIEVEMENTS_CREATE_TABLE);
		Log.e("Database created", "Database created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.e("Database model", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + DataSchema.REALMS_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DataSchema.CLASSES_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DataSchema.RACES_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DataSchema.ACHIEVEMENTS_CATEGORIES_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DataSchema.ACHIEVEMENTS_TABLE_NAME);
		onCreate(db);
	}
}