package com.sarth.wowarmory.models;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import com.sarth.wowarmory.Commons;

public class RaceInfo {

	public Long id;
	public int race_id;
	public String race_name;
	public String race_side;

	public RaceInfo() {
		this.id = 0l;
		this.race_id = 0;
		this.race_name = "";
		this.race_name = "";
	}

	public RaceInfo(Long id2, int rid, String rname, String rside) {
		this.id = id2;
		this.race_id = rid;
		this.race_name = rname;
		this.race_name = rside;
	}

	public RaceInfo(int int1, String string, String string2) {
		this.race_id = int1;
		this.race_name = string;
		this.race_side = string2;
	}

	public static ArrayList<RaceInfo> fromJson(JSONObject jsonGet) {
		ArrayList<RaceInfo> rri = new ArrayList<RaceInfo>();
		RaceInfo ri;
		try {
			JSONArray arr = jsonGet.getJSONArray("races");
			for (int i = 0; i < arr.length(); i++) {
				ri = new RaceInfo();
				JSONObject jsonRaces = arr.getJSONObject(i);
				ri.race_id = jsonRaces.getInt("id");
				Commons.INST.charRaceId = ri.race_id;
				ri.race_name = jsonRaces.getString("name");
				Commons.INST.charRace = ri.race_name;
				ri.race_side = jsonRaces.getString("side");
				Commons.INST.charSide = ri.race_side;
				rri.add(ri);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rri;
	}
}