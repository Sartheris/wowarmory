package com.sarth.wowarmory.models;

public class TalentItem {

	public String talentName, talentDescription, talentIconSlug;
	public String talentCastTime, talentCooldown, talentRange, talentPowerCost;
}