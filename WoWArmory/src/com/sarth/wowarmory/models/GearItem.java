package com.sarth.wowarmory.models;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import com.sarth.wowarmory.Commons;

public class GearItem {
	
	public int itemId, itemBind, buyPrice, itemClass, itemSubClass, containerSlots, inventoryType;
	public int itemLevel, maxDurability, minFactionId, minReputation, quality, sellPrice, requiredLevel;
	public int baseArmor, armor, displayInfoId, nameDescriptionColor;
	public String description, name, icon, nameDescription, itemSpell, itemSpellTrigger;
	public boolean hasSockets, upgradable, heroicTooltip, extraSocket;
	public ArrayList<ItemSingleStat> itemStat;
	public TooltipParamsInfo tooltipParamsInfo;
	public WeaponInfo wepInfo;
	public SocketInfo socketInfo;

	public static GearItem getAllEquippedItems(JSONObject jsonGet) {
		try {
			if (jsonGet != null && jsonGet.has("items")) {
				Commons.currentCharacter = new CharInfo();
				JSONObject jsonItems = new JSONObject();
				jsonItems = jsonGet.getJSONObject("items");
				if (jsonItems != null) {
					if (Commons.currentCharacter.gearItems == null) {
						Commons.currentCharacter.gearItems = new HashMap<String, GearItem>();
					}
					if (jsonItems.has("averageItemLevel")) {
						Commons.INST.averageItemLevel = jsonItems.getInt("averageItemLevel");
					}
					if (jsonItems.has("averageItemLevelEquipped")) {
						Commons.INST.averageItemLevelEquipped = jsonItems.getInt("averageItemLevelEquipped");
					}
					if (jsonItems.has("head")) {
						getEquippedItem("head", jsonItems);
					}
					if (jsonItems.has("neck")) {
						getEquippedItem("neck", jsonItems);
					}
					if (jsonItems.has("shoulder")) {
						getEquippedItem("shoulder", jsonItems);
					}
					if (jsonItems.has("back")) {
						getEquippedItem("back", jsonItems);
					}
					if (jsonItems.has("chest")) {
						getEquippedItem("chest", jsonItems);
					}
					if (jsonItems.has("shirt")) {
						getEquippedItem("shirt", jsonItems);
					}
					if (jsonItems.has("tabard")) {
						getEquippedItem("tabard", jsonItems);
					}
					if (jsonItems.has("wrist")) {
						getEquippedItem("wrist", jsonItems);
					}
					if (jsonItems.has("hands")) {
						getEquippedItem("hands", jsonItems);
					}
					if (jsonItems.has("waist")) {
						getEquippedItem("waist", jsonItems);
					}
					if (jsonItems.has("legs")) {
						getEquippedItem("legs", jsonItems);
					}
					if (jsonItems.has("feet")) {
						getEquippedItem("feet", jsonItems);
					}
					if (jsonItems.has("finger1")) {
						getEquippedItem("finger1", jsonItems);
					}
					if (jsonItems.has("finger2")) {
						getEquippedItem("finger2", jsonItems);
					}
					if (jsonItems.has("trinket1")) {
						getEquippedItem("trinket1", jsonItems);
					}
					if (jsonItems.has("trinket2")) {
						getEquippedItem("trinket2", jsonItems);
					}
					if (jsonItems.has("mainHand")) {
						getEquippedItem("mainHand", jsonItems);
					}
					if (jsonItems.has("offHand")) {
						getEquippedItem("offHand", jsonItems);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void getEquippedItem(String s, JSONObject jsonItems) {
		try {
			JSONObject currentJSONObject = jsonItems.getJSONObject(s);
			GearItem gi = new GearItem();
			gi.itemId = currentJSONObject.getInt("id");
			gi.name = currentJSONObject.getString("name");
			gi.icon = currentJSONObject.getString("icon");
			gi.quality = currentJSONObject.getInt("quality");
			gi.itemLevel = currentJSONObject.getInt("itemLevel");
			if (currentJSONObject.has("armor")) {
				gi.armor = currentJSONObject.getInt("armor");
			}
			/** tooltipParams */
			gi.tooltipParamsInfo = new TooltipParamsInfo();
			JSONObject jsGem = currentJSONObject.getJSONObject("tooltipParams");
			if (jsGem.has("gem0")) {
				gi.tooltipParamsInfo.gem0Id = jsGem.getInt("gem0");
			}
			if (jsGem.has("gem1")) {
				gi.tooltipParamsInfo.gem1Id = jsGem.getInt("gem1");
			}
			if (jsGem.has("gem2")) {
				gi.tooltipParamsInfo.gem2Id = jsGem.getInt("gem2");
			}
			if (jsGem.has("reforge")) {
				gi.tooltipParamsInfo.reforge = jsGem.getInt("reforge");
			}
			/** Weapon Info */
			if (currentJSONObject.has("weaponInfo")) {
				gi.wepInfo = new WeaponInfo();
				JSONObject jWep = currentJSONObject.getJSONObject("weaponInfo");
				gi.wepInfo.wepMinDmg = jWep.getJSONObject("damage").getInt("min");
				gi.wepInfo.wepMaxDmg = jWep.getJSONObject("damage").getInt("max");
				gi.wepInfo.wepSpeed = jWep.getDouble("weaponSpeed");
				gi.wepInfo.wepDps = jWep.getDouble("dps");
			}

			/** Extra Socket */
			gi.tooltipParamsInfo.iui = new ItemUpgradeInfo();
			if (currentJSONObject.getJSONObject("tooltipParams").has("extraSocket")) {
				gi.extraSocket = currentJSONObject.getJSONObject("tooltipParams").getBoolean("extraSocket");
			}
			/** Item Upgrade Info */
			if (currentJSONObject.getJSONObject("tooltipParams").has("upgrade")) {
				JSONObject jsUpgr = currentJSONObject.getJSONObject("tooltipParams").getJSONObject("upgrade");
				if (jsUpgr.has("current")) {
					gi.tooltipParamsInfo.iui.current = jsUpgr.getInt("current");
				}
				if (jsUpgr.has("total")) {
					gi.tooltipParamsInfo.iui.total = jsUpgr.getInt("total");
				}
			}
			/** stats */
			gi.itemStat = new ArrayList<ItemSingleStat>();
			JSONArray jArr = jsonItems.getJSONObject(s).getJSONArray("stats");
			for (int j = 0; j < jArr.length(); j++) {
				ItemSingleStat stat = new ItemSingleStat();
				JSONObject jObj = jArr.getJSONObject(j);
				stat.stat = jObj.getInt("stat");
				stat.amount = jObj.getInt("amount");
				if (jObj.has("reforgedAmount")) {
					stat.reforgedAmount = jObj.getInt("reforgedAmount");
				}
				if (jObj.has("reforged")) {
					stat.reforged = jObj.getBoolean("reforged");
				}
				gi.itemStat.add(stat);
			}
			Commons.currentCharacter.gearItems.put(s, gi);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static GearItem singleItemFromJson(JSONObject jsonGet) {
		if (Commons.currentCharacter.gearItems == null) {
			Commons.currentCharacter.gearItems = new HashMap<String, GearItem>();
		}
		if (jsonGet != null) {
			GearItem gi = Commons.currentCharacter.gearItems.get(Commons.INST.currentItem);
			if (gi == null) {
				gi = new GearItem();
			}
			try {
				gi.itemBind = jsonGet.getInt("itemBind");
				gi.icon = jsonGet.getString("icon");
				gi.hasSockets = jsonGet.getBoolean("hasSockets");
				gi.inventoryType = jsonGet.getInt("inventoryType");
				gi.requiredLevel = jsonGet.getInt("requiredLevel");
				gi.sellPrice = jsonGet.getInt("sellPrice");
				gi.maxDurability = jsonGet.getInt("maxDurability");
				gi.itemClass = jsonGet.getInt("maxDurability");
				gi.itemClass = jsonGet.getInt("itemClass");
				gi.itemSubClass = jsonGet.getInt("itemSubClass");
				gi.description = jsonGet.getString("description");
				gi.nameDescription = jsonGet.getString("nameDescription");
				/** Item Spells */
				JSONArray jj = jsonGet.getJSONArray("itemSpells");
				if (jj.length() > 0) {
					for (int i = 0; i < jj.length(); i++) {
						JSONObject jo = jj.getJSONObject(i).getJSONObject("spell");
						gi.itemSpell = jo.getString("description");
						gi.itemSpellTrigger = jj.getJSONObject(i).getString("trigger");
					}
				}
				/** Check number of sockets */
				gi.socketInfo = new SocketInfo();
				if (jsonGet.has("socketInfo") && jsonGet.getJSONObject("socketInfo").has("sockets")) {
					JSONArray jArr = jsonGet.getJSONObject("socketInfo").getJSONArray("sockets");
					gi.socketInfo.sockets = new Sockets();
					if (jArr.length() >= 1 && jArr.get(0) != null) {
						gi.socketInfo.sockets.numberOfSockets++;
						gi.socketInfo.sockets.socket0Type = jArr.getJSONObject(0).getString("type");
					}
					if (jArr.length() >= 2 && jArr.get(1) != null) {
						gi.socketInfo.sockets.numberOfSockets++;
						gi.socketInfo.sockets.socket1Type = jArr.getJSONObject(1).getString("type");
					}
					if (jArr.length() >= 3 && jArr.get(2) != null) {
						gi.socketInfo.sockets.numberOfSockets++;
						gi.socketInfo.sockets.socket2Type = jArr.getJSONObject(2).getString("type");
					}
					gi.socketInfo.socketBonus = jsonGet.getJSONObject("socketInfo").getString("socketBonus");
					Commons.currentCharacter.gearItems.put(Commons.INST.currentItem, gi);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}