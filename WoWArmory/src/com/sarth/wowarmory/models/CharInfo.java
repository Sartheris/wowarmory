package com.sarth.wowarmory.models;

import java.util.HashMap;
import org.json.JSONObject;
import com.sarth.wowarmory.Commons;

public class CharInfo {

	public HashMap<String, GearItem> gearItems;
	public HashMap<String, TalentItem> talentItem;

	public static CharInfo fromJson(JSONObject object) {
		CharInfo ci = new CharInfo();
		try {
			Commons.INST.charName = object.getString("name");
			Commons.INST.charRaceId = object.getInt("race");
			Commons.INST.charClassId = object.getInt("class");
			Commons.INST.charLevel = object.getInt("level");
			Commons.INST.charThumbnail = object.getString("thumbnail");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ci;
	}
}