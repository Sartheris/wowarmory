package com.sarth.wowarmory.models;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import com.sarth.wowarmory.Commons;

public class ClassInfo {

	public Long id;
	public int class_id;
	public String name;
	public String powerType;

	public ClassInfo() {
		this.id = 0l;
		this.class_id = 0;
		this.name = "";
		this.powerType = "";
	}

	public ClassInfo(Long id2, int cid, String cname, String cpowertype) {
		this.id = id2;
		this.class_id = cid;
		this.name = cname;
		this.powerType = cpowertype;
	}

	public ClassInfo(int id1, String name1, String power1) {
		this.class_id = id1;
		this.name = name1;
		this.powerType = power1;
	}

	public static ArrayList<ClassInfo> fromJson(JSONObject jsonGet) {
		ArrayList<ClassInfo> cci = new ArrayList<ClassInfo>();
		ClassInfo ci;
		try {
			JSONArray arr = jsonGet.getJSONArray("classes");
			for (int i = 0; i < arr.length(); i++) {
				ci = new ClassInfo();
				JSONObject jsonClasses = arr.getJSONObject(i);
				ci.class_id = jsonClasses.getInt("id");
				Commons.INST.charClassId = ci.class_id;
				ci.name = jsonClasses.getString("name");
				Commons.INST.charClass = ci.name;
				ci.powerType = jsonClasses.getString("powerType");
				Commons.INST.charPowerType = ci.powerType;
				cci.add(ci);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cci;
	}
}