package com.sarth.wowarmory.models;

public class TooltipParamsInfo {

	public String gem0, gem1, gem2;
	public String gem0Icon, gem1Icon, gem2Icon;
	public String gem0Type, gem1Type, gem2Type;
	public int gem0Id, gem1Id, gem2Id;
	public int reforge;
	public ItemUpgradeInfo iui;
}