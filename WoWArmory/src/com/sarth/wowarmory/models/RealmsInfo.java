package com.sarth.wowarmory.models;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;
import com.sarth.wowarmory.Commons;

public class RealmsInfo implements Parcelable {

	public Long id;
	public String realmRegion, realm, realmSlug;
	public boolean status;
	static ArrayList<RealmsInfo> realmInfo;

	public RealmsInfo() {
		this.id = 0l;
		this.realmRegion = "";
		this.realm = "";
		this.realmSlug = "";
	}

	public RealmsInfo(Long i, String rreg, String r, String ru) {
		this.id = i;
		this.realmRegion = rreg;
		this.realm = r;
		this.realmSlug = ru;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(realmRegion);
		dest.writeString(realm);
		dest.writeString(realmSlug);
		dest.writeInt(status ? 1 : 0); 
	}

	public static final Parcelable.Creator<RealmsInfo> CREATOR = new Parcelable.Creator<RealmsInfo>() {

		@Override
		public RealmsInfo createFromParcel(Parcel source) {
			RealmsInfo realm = new RealmsInfo();
			realm.realmRegion = source.readString();
			realm.realm = source.readString();
			realm.realmSlug = source.readString();
			realm.status = source.readInt() > 0 ? true : false;
			return realm;
		}

		@Override
		public RealmsInfo[] newArray(int size) {
			return new RealmsInfo[size];
		}
	};

	public static ArrayList<RealmsInfo> fromJson(JSONObject object) {
		try {
			realmInfo = new ArrayList<RealmsInfo>();
			JSONArray arr = object.getJSONArray("realms");
			for (int i = 0; i < arr.length(); i++) {
				JSONObject jsonRealm = arr.getJSONObject(i);
				RealmsInfo ri = new RealmsInfo();
				ri.realm = jsonRealm.getString("name");
				Commons.INST.realmName = ri.realm;
				ri.realmSlug = jsonRealm.getString("slug");
				Commons.INST.realmSlug = ri.realmSlug;
				ri.realmRegion = Commons.INST.regionUrl;
				ri.status = jsonRealm.getBoolean("status");
				realmInfo.add(ri);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realmInfo;
	}
}