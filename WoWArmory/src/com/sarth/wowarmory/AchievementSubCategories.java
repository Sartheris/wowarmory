package com.sarth.wowarmory;

import java.util.ArrayList;
import org.json.JSONObject;
import com.sarth.wowarmory.achievements.Achievement;
import com.sarth.wowarmory.achievements.AchievementCategory;
import com.sarth.wowarmory.achievements.CombinedAchivs;
import com.sarth.wowarmory.adapters.AchivsListAdapter;
import com.sarth.wowarmory.adapters.CombinedAchivsListAdapter;
import com.sarth.wowarmory.asynctasks.CurrentCharLoadingTask;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import com.sarth.wowarmory.sqlite.DataHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.support.v4.app.FragmentActivity;
import android.content.Intent;

public class AchievementSubCategories extends FragmentActivity implements JSONParserCallback {

	private ArrayList<AchievementCategory> arrAC;
	private ArrayList<Achievement> arrA;
	private LoadingDialogFragment loadingFragment;
	private JSONGet taskGet;
	private DataHelper dh;
	private int operationId;
	private long parentId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sub_categories);
		dh = new DataHelper(getApplicationContext());
		parentId = (Long) getIntent().getExtras().get("parentId");
		if (Commons.INST.charAchivsNeedUpdating) {
			loadingFragment = LoadingDialogFragment.newInstance("Loading character specific achievements...");
			loadingFragment.show(getSupportFragmentManager(), "tag");
			String url = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
					+ "?fields=achievements";
			taskGet = new JSONGet(this, url);
			taskGet.execute();
			operationId = 1;
		} else {
			initializeListViews();
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		switch (operationId) {
		case 1:
			operationId = 2;
			CurrentCharLoadingTask cc = new CurrentCharLoadingTask(this, this, jsonGet);
			cc.execute();
			break;
		case 2:
			Commons.INST.charAchivsNeedUpdating = false;
			initializeListViews();
			break;
		}
	}

	private void initializeListViews() {
		boolean b = dh.hasCategories(parentId);
		arrA = dh.selectAchivs(parentId);
		if (b) {
			arrAC = new ArrayList<AchievementCategory>();
			arrAC = dh.selectCategories(parentId);
			ArrayList<CombinedAchivs> arr = new ArrayList<CombinedAchivs>();
			for (int i = 0; i < arrAC.size(); i++) {
				arr.add(new CombinedAchivs(1, null, arrAC.get(i)));
			}
			for (int i = 0; i < arrA.size(); i++) {
				arr.add(new CombinedAchivs(2, arrA.get(i), null));
			}
			ListView lv = (ListView) findViewById(R.id.listViewAchivs);
			CombinedAchivsListAdapter adap = new CombinedAchivsListAdapter(this, android.R.layout.simple_spinner_dropdown_item, arr);
			lv.setAdapter(adap);
			lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					if (arg1.getId() == 1) {
						long tag = (Long) arrAC.get(arg2).Id;
						Intent i = new Intent(AchievementSubCategories.this, AchievementSubCategories.class);
						i.putExtra("parentId", tag);
						startActivity(i);
					}
				}
			});
		} else {
			ListView lv = (ListView) findViewById(R.id.listViewAchivs);
			AchivsListAdapter adap = new AchivsListAdapter(this, android.R.layout.simple_spinner_dropdown_item, arrA);
			lv.setAdapter(adap);
		}
		if (loadingFragment != null) {
			loadingFragment.dismiss();
		}
	}
}