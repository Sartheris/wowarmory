package com.sarth.wowarmory;

import org.json.JSONException;
import org.json.JSONObject;
import com.sarth.wowarmory.asynctasks.JSONGet;
import com.sarth.wowarmory.interfaces.JSONParserCallback;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Fragment7 extends Fragment implements JSONParserCallback {

	private JSONGet taskGet;
	private TextView tvTotalHonorableKills;
	private TextView tv2v2Rating, tv2v2WeeklyPlayed, tv2v2WeeklyWon, tv2v2WeeklyLost, tv2v2SeasonPlayed, tv2v2SeasonWon, tv2v2SeasonLost;
	private TextView tv3v3Rating, tv3v3WeeklyPlayed, tv3v3WeeklyWon, tv3v3WeeklyLost, tv3v3SeasonPlayed, tv3v3SeasonWon, tv3v3SeasonLost;
	private TextView tv5v5Rating, tv5v5WeeklyPlayed, tv5v5WeeklyWon, tv5v5WeeklyLost, tv5v5SeasonPlayed, tv5v5SeasonWon, tv5v5SeasonLost;
	private TextView tvrbgRating, tvrbgWeeklyPlayed, tvrbgWeeklyWon, tvrbgWeeklyLost, tvrbgSeasonPlayed, tvrbgSeasonWon, tvrbgSeasonLost;

	public Fragment7() {
		// Empty constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (Commons.INST.realmSlug == null) {
			super.getActivity().finish();
			return null;
		}
		View rootView = inflater.inflate(R.layout.fragment7, container, false);
		tvTotalHonorableKills = (TextView) rootView.findViewById(R.id.tvTotalKills);

		tv2v2Rating = (TextView) rootView.findViewById(R.id.tv2v2rating);
		tv2v2WeeklyPlayed = (TextView) rootView.findViewById(R.id.tv2v2weeklyplayed);
		tv2v2WeeklyWon = (TextView) rootView.findViewById(R.id.tv2v2weeklywon);
		tv2v2WeeklyLost = (TextView) rootView.findViewById(R.id.tv2v2weeklylost);
		tv2v2SeasonPlayed = (TextView) rootView.findViewById(R.id.tv2v2seasonplayed);
		tv2v2SeasonWon = (TextView) rootView.findViewById(R.id.tv2v2seasonwon);
		tv2v2SeasonLost = (TextView) rootView.findViewById(R.id.tv2v2seasonlost);

		tv3v3Rating = (TextView) rootView.findViewById(R.id.tv3v3rating);
		tv3v3WeeklyPlayed = (TextView) rootView.findViewById(R.id.tv3v3weeklyplayed);
		tv3v3WeeklyWon = (TextView) rootView.findViewById(R.id.tv3v3weeklywon);
		tv3v3WeeklyLost = (TextView) rootView.findViewById(R.id.tv3v3weeklylost);
		tv3v3SeasonPlayed = (TextView) rootView.findViewById(R.id.tv3v3seasonplayed);
		tv3v3SeasonWon = (TextView) rootView.findViewById(R.id.tv3v3seasonwon);
		tv3v3SeasonLost = (TextView) rootView.findViewById(R.id.tv3v3seasonlost);

		tv5v5Rating = (TextView) rootView.findViewById(R.id.tv5v5rating);
		tv5v5WeeklyPlayed = (TextView) rootView.findViewById(R.id.tv5v5weeklyplayed);
		tv5v5WeeklyWon = (TextView) rootView.findViewById(R.id.tv5v5weeklywon);
		tv5v5WeeklyLost = (TextView) rootView.findViewById(R.id.tv5v5weeklylost);
		tv5v5SeasonPlayed = (TextView) rootView.findViewById(R.id.tv5v5seasonplayed);
		tv5v5SeasonWon = (TextView) rootView.findViewById(R.id.tv5v5seasonwon);
		tv5v5SeasonLost = (TextView) rootView.findViewById(R.id.tv5v5seasonlost);

		tvrbgRating = (TextView) rootView.findViewById(R.id.tvrbgrating);
		tvrbgWeeklyPlayed = (TextView) rootView.findViewById(R.id.tvrbgweeklyplayed);
		tvrbgWeeklyWon = (TextView) rootView.findViewById(R.id.tvrbgweeklywon);
		tvrbgWeeklyLost = (TextView) rootView.findViewById(R.id.tvrbgweeklylost);
		tvrbgSeasonPlayed = (TextView) rootView.findViewById(R.id.tvrbgseasonplayed);
		tvrbgSeasonWon = (TextView) rootView.findViewById(R.id.tvrbgseasonwon);
		tvrbgSeasonLost = (TextView) rootView.findViewById(R.id.tvrbgseasonlost);

		return rootView;
	}

	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);
		if (menuVisible) {
			if (!Commons.INST.fragment7done) {
				String adres = "http://" + Commons.INST.regionUrl + ".battle.net/api/wow/character/" + Commons.INST.realmSlug + "/" + Commons.INST.charName
						+ "?fields=pvp" + "&locale=" + Commons.INST.locale;
				taskGet = new JSONGet(this, adres);
				taskGet.execute();
			}
		} else {
			if (taskGet != null) {
				taskGet.cancel(true);
			}
		}
	}

	@Override
	public void onFinishedParsing(JSONObject jsonGet) {
		try {
			tvTotalHonorableKills.setText(String.format(getString(R.string.pvp_total_kills), jsonGet.getString("totalHonorableKills")));
			JSONObject j = jsonGet.getJSONObject("pvp").getJSONObject("brackets");

			JSONObject j2 = j.getJSONObject("ARENA_BRACKET_2v2");
			tv2v2Rating.setText(String.format(getString(R.string.pvp_rating), j2.getString("rating")));
			tv2v2WeeklyPlayed.setText(String.format(getString(R.string.pvp_weekly_played), j2.getString("weeklyPlayed")));
			tv2v2WeeklyWon.setText(String.format(getString(R.string.pvp_weekly_won), j2.getString("weeklyWon")));
			tv2v2WeeklyLost.setText(String.format(getString(R.string.pvp_weekly_lost), j2.getString("weeklyLost")));
			tv2v2SeasonPlayed.setText(String.format(getString(R.string.pvp_season_played), j2.getString("seasonPlayed")));
			tv2v2SeasonWon.setText(String.format(getString(R.string.pvp_season_won), j2.getString("seasonWon")));
			tv2v2SeasonLost.setText(String.format(getString(R.string.pvp_season_lost), j2.getString("seasonLost")));

			JSONObject j3 = j.getJSONObject("ARENA_BRACKET_3v3");
			tv3v3Rating.setText(String.format(getString(R.string.pvp_rating), j3.getString("rating")));
			tv3v3WeeklyPlayed.setText(String.format(getString(R.string.pvp_weekly_played), j3.getString("weeklyPlayed")));
			tv3v3WeeklyWon.setText(String.format(getString(R.string.pvp_weekly_won), j3.getString("weeklyWon")));
			tv3v3WeeklyLost.setText(String.format(getString(R.string.pvp_weekly_lost), j3.getString("weeklyLost")));
			tv3v3SeasonPlayed.setText(String.format(getString(R.string.pvp_season_played), j3.getString("seasonPlayed")));
			tv3v3SeasonWon.setText(String.format(getString(R.string.pvp_season_won), j3.getString("seasonWon")));
			tv3v3SeasonLost.setText(String.format(getString(R.string.pvp_season_lost), j3.getString("seasonLost")));

			JSONObject j5 = j.getJSONObject("ARENA_BRACKED_5v5");
			tv5v5Rating.setText(String.format(getString(R.string.pvp_rating), j5.getString("rating")));
			tv5v5WeeklyPlayed.setText(String.format(getString(R.string.pvp_weekly_played), j5.getString("weeklyPlayed")));
			tv5v5WeeklyWon.setText(String.format(getString(R.string.pvp_weekly_won), j5.getString("weeklyWon")));
			tv5v5WeeklyLost.setText(String.format(getString(R.string.pvp_weekly_lost), j5.getString("weeklyLost")));
			tv5v5SeasonPlayed.setText(String.format(getString(R.string.pvp_season_played), j5.getString("seasonPlayed")));
			tv5v5SeasonWon.setText(String.format(getString(R.string.pvp_season_won), j5.getString("seasonWon")));
			tv5v5SeasonLost.setText(String.format(getString(R.string.pvp_season_lost), j5.getString("seasonLost")));

			JSONObject jr = j.getJSONObject("ARENA_BRACKET_RBG");
			tvrbgRating.setText(String.format(getString(R.string.pvp_rating), jr.getString("rating")));
			tvrbgWeeklyPlayed.setText(String.format(getString(R.string.pvp_weekly_played), jr.getString("weeklyPlayed")));
			tvrbgWeeklyWon.setText(String.format(getString(R.string.pvp_weekly_won), jr.getString("weeklyWon")));
			tvrbgWeeklyLost.setText(String.format(getString(R.string.pvp_weekly_lost), jr.getString("weeklyLost")));
			tvrbgSeasonPlayed.setText(String.format(getString(R.string.pvp_season_played), jr.getString("seasonPlayed")));
			tvrbgSeasonWon.setText(String.format(getString(R.string.pvp_season_won), jr.getString("seasonWon")));
			tvrbgSeasonLost.setText(String.format(getString(R.string.pvp_season_lost), jr.getString("seasonLost")));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Commons.INST.fragment7done = true;
	}
}